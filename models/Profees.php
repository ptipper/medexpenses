<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profees".
 */
class Profees extends \app\models\base\Profees
{
    // Insurance claim update action constants
    const CLAIM_UPDATEACTION_NONE = 0;
    const CLAIM_UPDATEACTION_ADD = 1;
    const CLAIM_UPDATEACTION_UPDATE = 2;
    const CLAIM_UPDATEACTION_REMOVE = 3;
    
    /**
     * @var integer Insurance claim update action
     */
    public $claimUpdateAction = self::CLAIM_UPDATEACTION_NONE;
}
