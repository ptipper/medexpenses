<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Families;

/**
 * FamiliesSearch represents the model behind the search form about Families.
 */
class FamiliesSearch extends Model
{
	public $familyid;
	public $familyname;
	public $policyid;
	public $policy;

	public function rules()
	{
		return [
			[['familyid', 'policyid'], 'integer'],
			[['familyname', 'policy'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'familyid' => 'Family id',
			'familyname' => 'Family name',
			'policyid' => 'Insurance policy id',
		];
	}

	public function search($params)
	{
		$query = Families::find();
		$query->joinWith('policy', true, 'LEFT JOIN');
		
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		// Allow sorting by insurance policy description
		$dataProvider->sort->attributes['policy'] = [
		    // The tables are the ones our relation are configured to
		    'asc' => ['inspolicies.description' => SORT_ASC],
		    'desc' => ['inspolicies.description' => SORT_DESC],
		];

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}
		
		$query->andFilterWhere([
            'familyid' => $this->familyid,
            'policyid' => $this->policyid,
        ]);

		$query->andFilterWhere(['like', 'familyname', $this->familyname])
		      ->andFilterWhere(['like', 'inspolicies.description', $this->policy]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
