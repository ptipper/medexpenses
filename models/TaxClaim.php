<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\models\Families;


/**
 * TaxClaim is the model for generating annual tax claims on medical expenses
 */
class TaxClaim extends Model
{
    /**
     * Tax year
     * @var integer
     */
    public $taxYear;
    
    /**
     * Family id
     * @var integer
     */
    public $familyId;
    
    /**
     * General medical expenses
     * @var float
     */
    public $genMedExpenses;
    
    /**
     * Prescription charges
     * @var float
     */
    public $prescriptions;
    
    /**
     * General medical expenses
     * @var float
     */
    public $totalMedExpenses;
    
    /**
     * Total paid by insurance
     * @var float
     */
    public $paidByInsurance;
    
    /**
     * Tax claim amount
     * @var float
     */
    public $taxClaimAmount;
    
    /**
     * Constructor
     */
    function __construct($taxYear = 0, $familyId = 0)
    {
        $this->taxYear = $taxYear;
        $this->familyId = $familyId;
        $this->genMedExpenses = 0.0;
        $this->prescriptions = 0.0;
        $this->totalMedExpenses = 0.0;
        $this->paidByInsurance = 0.0;
        $this->taxClaimAmount = 0.0;
        
    }
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // The tax year is required
            [['taxYear','familyId'],'required'],
            // Value properties
            [['genMedExpenses','prescriptions','totalMedExpenses','paidByInsurance', 'taxClaimAmount'], 'number'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'taxYear' => 'Tax year',
            'familyId' => 'Family id',
            'genMedExpenses' => 'General medical expenses',
            'prescriptions' => 'Prescriptions',
            'totalMedExpenses' => 'Total medical expenses',
            'paidByInsurance' => 'Paid by insurance',
            'taxClaimAmount' => 'Tax claim amount',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamily()
    {
        return Families::find()->where(['familyid' => $this->familyId])->one();
    }

    /**
     * Calculate the tax claim values
     */
    public function calculateTaxClaim()
    {
        if ($this->familyId == 0) {
            $returnCode = false;
        }
        else {
            $this->calculateGenMedExpenses();
            $this->calculatePrescriptionCharges();
            $this->totalMedExpenses = $this->genMedExpenses + $this->prescriptions;
            $this->taxClaimAmount = $this->totalMedExpenses - $this->paidByInsurance;
            $returnCode = true;
            
        }
        
        return $returnCode;
        
    }
    
    /**
     * Calculate general medical expenses and the total insurance benefit paid against them
     */
    private function calculateGenMedExpenses()
    {
        $genMedExpenses = 0.0;
        $paidByInsurance = 0.0;
        $query = new Query();
        
        $startDate = $this->taxYear . "-01-01";
        $endDate = $this->taxYear . "-12-31";
        
        // Build the query
        $query->select([
                'taxexptypeid' => 'taxexptypes.taxexptypeid',
                'description' => 'taxexptypes.description',
                'totalvalue' => 'SUM(profees.chargevalue)',
                'inspayment' => 'SUM(profees.inspayment)',
            ])
            ->from('profees')
            ->innerJoin('practitioners', 'practitioners.practitionerid = profees.practitionerid')
            ->innerJoin('familymembers', 'familymembers.memberid = profees.familymemberid')
            ->innerJoin('practtypes', 'practtypes.practtypeid = practitioners.practtypeid')
            ->innerJoin('taxexptypes', 'taxexptypes.taxexptypeid = practtypes.taxexptypeid')
            ->where([
                'familymembers.familyid' => $this->familyId,
            ])
            ->andWhere([
                'profees.taxclaimstatus' => 'Not claimed',
            ])
            ->andWhere(['>=','profees.chargedate',$startDate])
            ->andWhere(['<=','profees.chargedate',$endDate])
            ->andWhere([
                'taxexptypes.taxrelief' => true,
            ])
            ->groupBy('taxexptypes.taxexptypeid, taxexptypes.description')
            ->orderBy('taxexptypes.taxexptypeid')
            ;
            
            // Execute the query and save the results in the ChargeSummaryItem objects array
            $results = $query->all();
            
            foreach($results as $summaryRow) {
    
                $genMedExpenses += $summaryRow['totalvalue'];
                $paidByInsurance += $summaryRow['inspayment'];
            }
            
            $this->genMedExpenses = $genMedExpenses;
            $this->paidByInsurance = $paidByInsurance;
            
    }

    private function calculatePrescriptionCharges()
    {
        $prescriptionCharges = 0.0;
        $query = new Query();
        
        $startDate = $this->taxYear . "-01-01";
        $endDate = $this->taxYear . "-12-31";
        
        // Build the query
        $query->select([
                'familyid' => 'familymembers.familyid',
                'totalvalue' => 'SUM(prescriptions.chargevalue)',
            ])
            ->from('prescriptions')
            ->innerJoin('familymembers', 'familymembers.memberid = prescriptions.familymemberid')
            ->where([
                'familymembers.familyid' => $this->familyId,
            ])
            ->andWhere([
                'prescriptions.taxclaimstatus' => 'Not claimed',
            ])
            ->andWhere(['>=','prescriptions.chargedate',$startDate])
            ->andWhere(['<=','prescriptions.chargedate',$endDate])
            ->groupBy('familymembers.familyid')
            ->orderBy('familymembers.familyid')
            ;
            
            // Execute the query and save the results in the ChargeSummaryItem objects array
            $results = $query->all();
            
            foreach($results as $summaryRow) {
    
                $prescriptionCharges += $summaryRow['totalvalue'];
            }
            
            $this->prescriptions = $prescriptionCharges;
            
    }

    
}

?>