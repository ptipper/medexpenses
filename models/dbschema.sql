--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.2
-- Dumped by pg_dump version 9.2.2
-- Started on 2016-04-02 14:48:34

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

-- Comment the DROP DATABASE statement to avoid the risk of the database being blatted accidently
-- DROP DATABASE medexpenses;
--
-- TOC entry 2086 (class 1262 OID 53394)
-- Name: medexpenses; Type: DATABASE; Schema: -; Owner: ptipper
--

CREATE DATABASE medexpenses WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';


ALTER DATABASE medexpenses OWNER TO ptipper;

\connect medexpenses

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 2087 (class 1262 OID 53394)
-- Dependencies: 2086
-- Name: medexpenses; Type: COMMENT; Schema: -; Owner: ptipper
--

COMMENT ON DATABASE medexpenses IS 'Medical expenses database';


--
-- TOC entry 6 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 2088 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 194 (class 3079 OID 11727)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2090 (class 0 OID 0)
-- Dependencies: 194
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 537 (class 1247 OID 53396)
-- Name: claimstatus; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE claimstatus AS ENUM (
    'Not claimed',
    'Claimed',
    'Paid',
    'Rejected'
);


ALTER TYPE public.claimstatus OWNER TO postgres;

--
-- TOC entry 2091 (class 0 OID 0)
-- Dependencies: 537
-- Name: TYPE claimstatus; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TYPE claimstatus IS 'Claim status';


--
-- TOC entry 540 (class 1247 OID 53406)
-- Name: practitioner_title; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE practitioner_title AS ENUM (
    'Dr.',
    'Mr.',
    'Ms.',
    'Prof.',
    'N/A'
);


ALTER TYPE public.practitioner_title OWNER TO postgres;

--
-- TOC entry 2092 (class 0 OID 0)
-- Dependencies: 540
-- Name: TYPE practitioner_title; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TYPE practitioner_title IS 'Practitioner title';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 168 (class 1259 OID 53415)
-- Name: charges; Type: TABLE; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE TABLE charges (
    chargeid integer NOT NULL,
    familymemberid integer,
    chargedate date,
    chargevalue numeric(10,2),
    notes text,
    receiptimage bytea,
    taxclaimstatus claimstatus,
    inspayment numeric(10,2),
    taxrelief numeric(10,2)
);


ALTER TABLE public.charges OWNER TO ptipper;

--
-- TOC entry 2093 (class 0 OID 0)
-- Dependencies: 168
-- Name: TABLE charges; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON TABLE charges IS 'Medical charges';


--
-- TOC entry 2094 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN charges.chargeid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN charges.chargeid IS 'Charge id';


--
-- TOC entry 2095 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN charges.familymemberid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN charges.familymemberid IS 'Family member';


--
-- TOC entry 2096 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN charges.chargedate; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN charges.chargedate IS 'Charge date';


--
-- TOC entry 2097 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN charges.chargevalue; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN charges.chargevalue IS 'Charge value';


--
-- TOC entry 2098 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN charges.notes; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN charges.notes IS 'Notes';


--
-- TOC entry 2099 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN charges.receiptimage; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN charges.receiptimage IS 'Charge receipt image';


--
-- TOC entry 2100 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN charges.taxclaimstatus; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN charges.taxclaimstatus IS 'Tax relief claim status';


--
-- TOC entry 2101 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN charges.inspayment; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN charges.inspayment IS 'Amount paid out by insurance company';


--
-- TOC entry 2102 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN charges.taxrelief; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN charges.taxrelief IS 'Tax relief granted';


--
-- TOC entry 169 (class 1259 OID 53421)
-- Name: charges_chargeid_seq; Type: SEQUENCE; Schema: public; Owner: ptipper
--

CREATE SEQUENCE charges_chargeid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.charges_chargeid_seq OWNER TO ptipper;

--
-- TOC entry 2103 (class 0 OID 0)
-- Dependencies: 169
-- Name: charges_chargeid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ptipper
--

ALTER SEQUENCE charges_chargeid_seq OWNED BY charges.chargeid;


--
-- TOC entry 170 (class 1259 OID 53423)
-- Name: families; Type: TABLE; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE TABLE families (
    familyid integer NOT NULL,
    familyname character varying(120),
    policyid integer
);


ALTER TABLE public.families OWNER TO ptipper;

--
-- TOC entry 2104 (class 0 OID 0)
-- Dependencies: 170
-- Name: TABLE families; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON TABLE families IS 'Families';


--
-- TOC entry 2105 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN families.familyid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN families.familyid IS 'Family id';


--
-- TOC entry 2106 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN families.familyname; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN families.familyname IS 'Family name';


--
-- TOC entry 2107 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN families.policyid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN families.policyid IS 'Insurance policy id';


--
-- TOC entry 171 (class 1259 OID 53426)
-- Name: families_familyid_seq; Type: SEQUENCE; Schema: public; Owner: ptipper
--

CREATE SEQUENCE families_familyid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.families_familyid_seq OWNER TO ptipper;

--
-- TOC entry 2108 (class 0 OID 0)
-- Dependencies: 171
-- Name: families_familyid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ptipper
--

ALTER SEQUENCE families_familyid_seq OWNED BY families.familyid;


--
-- TOC entry 172 (class 1259 OID 53428)
-- Name: familymembers; Type: TABLE; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE TABLE familymembers (
    memberid integer NOT NULL,
    familyid integer,
    name character varying(120)
);


ALTER TABLE public.familymembers OWNER TO ptipper;

--
-- TOC entry 2109 (class 0 OID 0)
-- Dependencies: 172
-- Name: TABLE familymembers; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON TABLE familymembers IS 'Family members';


--
-- TOC entry 2110 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN familymembers.memberid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN familymembers.memberid IS 'Family member id';


--
-- TOC entry 2111 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN familymembers.familyid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN familymembers.familyid IS 'Family id';


--
-- TOC entry 173 (class 1259 OID 53431)
-- Name: familymembers_memberid_seq; Type: SEQUENCE; Schema: public; Owner: ptipper
--

CREATE SEQUENCE familymembers_memberid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.familymembers_memberid_seq OWNER TO ptipper;

--
-- TOC entry 2112 (class 0 OID 0)
-- Dependencies: 173
-- Name: familymembers_memberid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ptipper
--

ALTER SEQUENCE familymembers_memberid_seq OWNED BY familymembers.memberid;


--
-- TOC entry 174 (class 1259 OID 53433)
-- Name: insccrules; Type: TABLE; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE TABLE insccrules (
    claimcodeid integer NOT NULL,
    policyid integer NOT NULL,
    maxvisits smallint,
    maxclaimvalue numeric(10,2),
    percentcover numeric(5,2),
    maxpaypppa numeric(10,2)
);


ALTER TABLE public.insccrules OWNER TO ptipper;

--
-- TOC entry 2113 (class 0 OID 0)
-- Dependencies: 174
-- Name: TABLE insccrules; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON TABLE insccrules IS 'Insurance policy rules';


--
-- TOC entry 2114 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN insccrules.claimcodeid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insccrules.claimcodeid IS 'Claim code id';


--
-- TOC entry 2115 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN insccrules.policyid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insccrules.policyid IS 'Insurance policy id';


--
-- TOC entry 2116 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN insccrules.maxvisits; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insccrules.maxvisits IS 'Maximum number of visits per claim';


--
-- TOC entry 2117 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN insccrules.maxclaimvalue; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insccrules.maxclaimvalue IS 'Maximum payable per person per visit';


--
-- TOC entry 2118 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN insccrules.percentcover; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insccrules.percentcover IS 'Percentage cover';


--
-- TOC entry 2119 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN insccrules.maxpaypppa; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insccrules.maxpaypppa IS 'Maximum amount payable per person per annum';


--
-- TOC entry 175 (class 1259 OID 53436)
-- Name: insclaimcodes; Type: TABLE; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE TABLE insclaimcodes (
    claimcodeid integer NOT NULL,
    insurancecoid integer,
    claimcode character(4),
    description character varying(120),
    maxvisits smallint,
    maxclaimvalue money,
    percentcover numeric(5,2)
);


ALTER TABLE public.insclaimcodes OWNER TO ptipper;

--
-- TOC entry 2120 (class 0 OID 0)
-- Dependencies: 175
-- Name: TABLE insclaimcodes; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON TABLE insclaimcodes IS 'Insurance claim codes';


--
-- TOC entry 2121 (class 0 OID 0)
-- Dependencies: 175
-- Name: COLUMN insclaimcodes.claimcodeid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insclaimcodes.claimcodeid IS 'Claim code id';


--
-- TOC entry 2122 (class 0 OID 0)
-- Dependencies: 175
-- Name: COLUMN insclaimcodes.insurancecoid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insclaimcodes.insurancecoid IS 'Insurance company id';


--
-- TOC entry 2123 (class 0 OID 0)
-- Dependencies: 175
-- Name: COLUMN insclaimcodes.claimcode; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insclaimcodes.claimcode IS 'Insurance claim code';


--
-- TOC entry 2124 (class 0 OID 0)
-- Dependencies: 175
-- Name: COLUMN insclaimcodes.maxvisits; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insclaimcodes.maxvisits IS 'Maximum number of visits per claim';


--
-- TOC entry 2125 (class 0 OID 0)
-- Dependencies: 175
-- Name: COLUMN insclaimcodes.maxclaimvalue; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insclaimcodes.maxclaimvalue IS 'Maximum claim value';


--
-- TOC entry 2126 (class 0 OID 0)
-- Dependencies: 175
-- Name: COLUMN insclaimcodes.percentcover; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insclaimcodes.percentcover IS 'Percentage cover';


--
-- TOC entry 176 (class 1259 OID 53439)
-- Name: insclaimcodes_claimcodeid_seq; Type: SEQUENCE; Schema: public; Owner: ptipper
--

CREATE SEQUENCE insclaimcodes_claimcodeid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.insclaimcodes_claimcodeid_seq OWNER TO ptipper;

--
-- TOC entry 2127 (class 0 OID 0)
-- Dependencies: 176
-- Name: insclaimcodes_claimcodeid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ptipper
--

ALTER SEQUENCE insclaimcodes_claimcodeid_seq OWNED BY insclaimcodes.claimcodeid;


--
-- TOC entry 177 (class 1259 OID 53441)
-- Name: insclaims; Type: TABLE; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE TABLE insclaims (
    claimid integer NOT NULL,
    claimref character varying(20),
    claimdate date,
    startdate date,
    enddate date,
    settled boolean,
    policyid integer NOT NULL
);


ALTER TABLE public.insclaims OWNER TO ptipper;

--
-- TOC entry 2128 (class 0 OID 0)
-- Dependencies: 177
-- Name: TABLE insclaims; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON TABLE insclaims IS 'Insurance claims';


--
-- TOC entry 2129 (class 0 OID 0)
-- Dependencies: 177
-- Name: COLUMN insclaims.claimid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insclaims.claimid IS 'Insurance claim id';


--
-- TOC entry 2130 (class 0 OID 0)
-- Dependencies: 177
-- Name: COLUMN insclaims.claimref; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insclaims.claimref IS 'Claim reference';


--
-- TOC entry 2131 (class 0 OID 0)
-- Dependencies: 177
-- Name: COLUMN insclaims.claimdate; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insclaims.claimdate IS 'Date claim submitted';


--
-- TOC entry 2132 (class 0 OID 0)
-- Dependencies: 177
-- Name: COLUMN insclaims.startdate; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insclaims.startdate IS 'Start date of claim period';


--
-- TOC entry 2133 (class 0 OID 0)
-- Dependencies: 177
-- Name: COLUMN insclaims.enddate; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insclaims.enddate IS 'End date of claim period';


--
-- TOC entry 2134 (class 0 OID 0)
-- Dependencies: 177
-- Name: COLUMN insclaims.settled; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insclaims.settled IS 'Claim settled';


--
-- TOC entry 2135 (class 0 OID 0)
-- Dependencies: 177
-- Name: COLUMN insclaims.policyid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insclaims.policyid IS 'Insurance policy id';


--
-- TOC entry 178 (class 1259 OID 53444)
-- Name: insclaims_claimid_seq; Type: SEQUENCE; Schema: public; Owner: ptipper
--

CREATE SEQUENCE insclaims_claimid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.insclaims_claimid_seq OWNER TO ptipper;

--
-- TOC entry 2136 (class 0 OID 0)
-- Dependencies: 178
-- Name: insclaims_claimid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ptipper
--

ALTER SEQUENCE insclaims_claimid_seq OWNED BY insclaims.claimid;


--
-- TOC entry 179 (class 1259 OID 53446)
-- Name: inspolicies; Type: TABLE; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE TABLE inspolicies (
    policyid integer NOT NULL,
    insurancecoid integer,
    policyno character(20),
    description character varying(120)
);


ALTER TABLE public.inspolicies OWNER TO ptipper;

--
-- TOC entry 2137 (class 0 OID 0)
-- Dependencies: 179
-- Name: TABLE inspolicies; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON TABLE inspolicies IS 'Insurance policies';


--
-- TOC entry 2138 (class 0 OID 0)
-- Dependencies: 179
-- Name: COLUMN inspolicies.policyid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN inspolicies.policyid IS 'Insurance policy id';


--
-- TOC entry 2139 (class 0 OID 0)
-- Dependencies: 179
-- Name: COLUMN inspolicies.insurancecoid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN inspolicies.insurancecoid IS 'Insurance company id';


--
-- TOC entry 2140 (class 0 OID 0)
-- Dependencies: 179
-- Name: COLUMN inspolicies.policyno; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN inspolicies.policyno IS 'Policy number';


--
-- TOC entry 2141 (class 0 OID 0)
-- Dependencies: 179
-- Name: COLUMN inspolicies.description; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN inspolicies.description IS 'Policy description';


--
-- TOC entry 180 (class 1259 OID 53449)
-- Name: inspolicies_policyid_seq; Type: SEQUENCE; Schema: public; Owner: ptipper
--

CREATE SEQUENCE inspolicies_policyid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inspolicies_policyid_seq OWNER TO ptipper;

--
-- TOC entry 2142 (class 0 OID 0)
-- Dependencies: 180
-- Name: inspolicies_policyid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ptipper
--

ALTER SEQUENCE inspolicies_policyid_seq OWNED BY inspolicies.policyid;


--
-- TOC entry 181 (class 1259 OID 53451)
-- Name: insurancecos; Type: TABLE; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE TABLE insurancecos (
    insurancecoid integer NOT NULL,
    name character varying(120)
);


ALTER TABLE public.insurancecos OWNER TO ptipper;

--
-- TOC entry 2143 (class 0 OID 0)
-- Dependencies: 181
-- Name: TABLE insurancecos; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON TABLE insurancecos IS 'Insurance companies';


--
-- TOC entry 2144 (class 0 OID 0)
-- Dependencies: 181
-- Name: COLUMN insurancecos.insurancecoid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insurancecos.insurancecoid IS 'Insurance company id';


--
-- TOC entry 2145 (class 0 OID 0)
-- Dependencies: 181
-- Name: COLUMN insurancecos.name; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN insurancecos.name IS 'Company name';


--
-- TOC entry 182 (class 1259 OID 53454)
-- Name: insurancecos_insurancecoid_seq; Type: SEQUENCE; Schema: public; Owner: ptipper
--

CREATE SEQUENCE insurancecos_insurancecoid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.insurancecos_insurancecoid_seq OWNER TO ptipper;

--
-- TOC entry 2146 (class 0 OID 0)
-- Dependencies: 182
-- Name: insurancecos_insurancecoid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ptipper
--

ALTER SEQUENCE insurancecos_insurancecoid_seq OWNED BY insurancecos.insurancecoid;


--
-- TOC entry 183 (class 1259 OID 53456)
-- Name: pharmacies; Type: TABLE; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE TABLE pharmacies (
    pharmacyid integer NOT NULL,
    name character varying(120)
);


ALTER TABLE public.pharmacies OWNER TO ptipper;

--
-- TOC entry 2147 (class 0 OID 0)
-- Dependencies: 183
-- Name: TABLE pharmacies; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON TABLE pharmacies IS 'Pharmacists';


--
-- TOC entry 2148 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN pharmacies.pharmacyid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN pharmacies.pharmacyid IS 'Pharmacy id';


--
-- TOC entry 2149 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN pharmacies.name; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN pharmacies.name IS 'Pharmacy name';


--
-- TOC entry 184 (class 1259 OID 53459)
-- Name: pharmacies_pharmacyid_seq; Type: SEQUENCE; Schema: public; Owner: ptipper
--

CREATE SEQUENCE pharmacies_pharmacyid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pharmacies_pharmacyid_seq OWNER TO ptipper;

--
-- TOC entry 2150 (class 0 OID 0)
-- Dependencies: 184
-- Name: pharmacies_pharmacyid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ptipper
--

ALTER SEQUENCE pharmacies_pharmacyid_seq OWNED BY pharmacies.pharmacyid;


--
-- TOC entry 185 (class 1259 OID 53461)
-- Name: practitioners; Type: TABLE; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE TABLE practitioners (
    practitionerid integer NOT NULL,
    title practitioner_title,
    firstname character varying(20),
    surname character varying(80),
    practtypeid integer
);


ALTER TABLE public.practitioners OWNER TO ptipper;

--
-- TOC entry 2151 (class 0 OID 0)
-- Dependencies: 185
-- Name: TABLE practitioners; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON TABLE practitioners IS 'Medical practitioners';


--
-- TOC entry 2152 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN practitioners.practitionerid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN practitioners.practitionerid IS 'Practitioner id';


--
-- TOC entry 2153 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN practitioners.title; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN practitioners.title IS 'Title';


--
-- TOC entry 2154 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN practitioners.firstname; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN practitioners.firstname IS 'First name';


--
-- TOC entry 2155 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN practitioners.surname; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN practitioners.surname IS 'Surname';


--
-- TOC entry 2156 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN practitioners.practtypeid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN practitioners.practtypeid IS 'Practitioner type';


--
-- TOC entry 186 (class 1259 OID 53464)
-- Name: practitioners_practitionerid_seq; Type: SEQUENCE; Schema: public; Owner: ptipper
--

CREATE SEQUENCE practitioners_practitionerid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.practitioners_practitionerid_seq OWNER TO ptipper;

--
-- TOC entry 2157 (class 0 OID 0)
-- Dependencies: 186
-- Name: practitioners_practitionerid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ptipper
--

ALTER SEQUENCE practitioners_practitionerid_seq OWNED BY practitioners.practitionerid;


--
-- TOC entry 187 (class 1259 OID 53466)
-- Name: practtypes; Type: TABLE; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE TABLE practtypes (
    practtypeid integer NOT NULL,
    description character varying(120),
    claimcodeid integer,
    taxexptypeid integer
);


ALTER TABLE public.practtypes OWNER TO ptipper;

--
-- TOC entry 2158 (class 0 OID 0)
-- Dependencies: 187
-- Name: TABLE practtypes; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON TABLE practtypes IS 'Practitioner types';


--
-- TOC entry 2159 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN practtypes.practtypeid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN practtypes.practtypeid IS 'Practitioner type id';


--
-- TOC entry 2160 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN practtypes.description; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN practtypes.description IS 'Description';


--
-- TOC entry 2161 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN practtypes.claimcodeid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN practtypes.claimcodeid IS 'Insurance claim code id';


--
-- TOC entry 2162 (class 0 OID 0)
-- Dependencies: 187
-- Name: COLUMN practtypes.taxexptypeid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN practtypes.taxexptypeid IS 'Tax expense type id';


--
-- TOC entry 188 (class 1259 OID 53469)
-- Name: practtypes_practtypeid_seq; Type: SEQUENCE; Schema: public; Owner: ptipper
--

CREATE SEQUENCE practtypes_practtypeid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.practtypes_practtypeid_seq OWNER TO ptipper;

--
-- TOC entry 2163 (class 0 OID 0)
-- Dependencies: 188
-- Name: practtypes_practtypeid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ptipper
--

ALTER SEQUENCE practtypes_practtypeid_seq OWNED BY practtypes.practtypeid;


--
-- TOC entry 189 (class 1259 OID 53471)
-- Name: prescriptions; Type: TABLE; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE TABLE prescriptions (
    pharmacyid integer,
    formno character(20)
)
INHERITS (charges);


ALTER TABLE public.prescriptions OWNER TO ptipper;

--
-- TOC entry 2164 (class 0 OID 0)
-- Dependencies: 189
-- Name: TABLE prescriptions; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON TABLE prescriptions IS 'Prescriptions';


--
-- TOC entry 2165 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN prescriptions.pharmacyid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN prescriptions.pharmacyid IS 'Pharmacy';


--
-- TOC entry 2166 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN prescriptions.formno; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN prescriptions.formno IS 'Prescription form number';


--
-- TOC entry 190 (class 1259 OID 53477)
-- Name: profees; Type: TABLE; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE TABLE profees (
    practitionerid integer,
    insclaimid integer,
    claimable boolean,
    insclaimstatus claimstatus
)
INHERITS (charges);


ALTER TABLE public.profees OWNER TO ptipper;

--
-- TOC entry 2167 (class 0 OID 0)
-- Dependencies: 190
-- Name: TABLE profees; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON TABLE profees IS 'Professional fees';


--
-- TOC entry 2168 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN profees.practitionerid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN profees.practitionerid IS 'Medical practitioner';


--
-- TOC entry 2169 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN profees.insclaimid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN profees.insclaimid IS 'Insurance claim id';


--
-- TOC entry 2170 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN profees.claimable; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN profees.claimable IS 'Fee claimable from insurance';


--
-- TOC entry 2171 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN profees.insclaimstatus; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN profees.insclaimstatus IS 'Insurance claim status';


--
-- TOC entry 191 (class 1259 OID 53483)
-- Name: taxexptypes; Type: TABLE; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE TABLE taxexptypes (
    taxexptypeid integer NOT NULL,
    description character varying(120),
    category character(10),
    section smallint,
    taxrelief boolean DEFAULT true
);


ALTER TABLE public.taxexptypes OWNER TO ptipper;

--
-- TOC entry 2172 (class 0 OID 0)
-- Dependencies: 191
-- Name: TABLE taxexptypes; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON TABLE taxexptypes IS 'Taxation expense types';


--
-- TOC entry 2173 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN taxexptypes.taxexptypeid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN taxexptypes.taxexptypeid IS 'Taxation expense type id';


--
-- TOC entry 2174 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN taxexptypes.description; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN taxexptypes.description IS 'Description';


--
-- TOC entry 2175 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN taxexptypes.category; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN taxexptypes.category IS 'Category code';


--
-- TOC entry 2176 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN taxexptypes.section; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN taxexptypes.section IS 'Section number';


--
-- TOC entry 2177 (class 0 OID 0)
-- Dependencies: 191
-- Name: COLUMN taxexptypes.taxrelief; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN taxexptypes.taxrelief IS 'Eligible for tax relief';


--
-- TOC entry 192 (class 1259 OID 53486)
-- Name: taxexptypes_taxexptypeid_seq; Type: SEQUENCE; Schema: public; Owner: ptipper
--

CREATE SEQUENCE taxexptypes_taxexptypeid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taxexptypes_taxexptypeid_seq OWNER TO ptipper;

--
-- TOC entry 2178 (class 0 OID 0)
-- Dependencies: 192
-- Name: taxexptypes_taxexptypeid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ptipper
--

ALTER SEQUENCE taxexptypes_taxexptypeid_seq OWNED BY taxexptypes.taxexptypeid;


--
-- TOC entry 193 (class 1259 OID 130538)
-- Name: tempfees; Type: TABLE; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE TABLE tempfees (
    chargeid integer DEFAULT nextval('charges_chargeid_seq'::regclass) NOT NULL,
    familymemberid integer,
    chargedate date,
    chargevalue numeric(10,2),
    notes text,
    receiptimage bytea,
    taxclaimstatus claimstatus,
    inspayment numeric(10,2),
    taxrelief numeric(10,2),
    practitionerid integer,
    insclaimid integer,
    claimable boolean,
    insclaimstatus claimstatus
);


ALTER TABLE public.tempfees OWNER TO ptipper;

--
-- TOC entry 2179 (class 0 OID 0)
-- Dependencies: 193
-- Name: TABLE tempfees; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON TABLE tempfees IS 'Temporary copy of profees table, for importing fees to insurance and tax claims.';


--
-- TOC entry 2180 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN tempfees.practitionerid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN tempfees.practitionerid IS 'Medical practitioner';


--
-- TOC entry 2181 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN tempfees.insclaimid; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN tempfees.insclaimid IS 'Insurance claim id';


--
-- TOC entry 2182 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN tempfees.claimable; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN tempfees.claimable IS 'Fee claimable from insurance';


--
-- TOC entry 2183 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN tempfees.insclaimstatus; Type: COMMENT; Schema: public; Owner: ptipper
--

COMMENT ON COLUMN tempfees.insclaimstatus IS 'Insurance claim status';


--
-- TOC entry 2002 (class 2604 OID 53495)
-- Name: chargeid; Type: DEFAULT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY charges ALTER COLUMN chargeid SET DEFAULT nextval('charges_chargeid_seq'::regclass);


--
-- TOC entry 2003 (class 2604 OID 53496)
-- Name: familyid; Type: DEFAULT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY families ALTER COLUMN familyid SET DEFAULT nextval('families_familyid_seq'::regclass);


--
-- TOC entry 2004 (class 2604 OID 53497)
-- Name: memberid; Type: DEFAULT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY familymembers ALTER COLUMN memberid SET DEFAULT nextval('familymembers_memberid_seq'::regclass);


--
-- TOC entry 2005 (class 2604 OID 53498)
-- Name: claimcodeid; Type: DEFAULT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY insclaimcodes ALTER COLUMN claimcodeid SET DEFAULT nextval('insclaimcodes_claimcodeid_seq'::regclass);


--
-- TOC entry 2006 (class 2604 OID 53499)
-- Name: claimid; Type: DEFAULT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY insclaims ALTER COLUMN claimid SET DEFAULT nextval('insclaims_claimid_seq'::regclass);


--
-- TOC entry 2007 (class 2604 OID 53500)
-- Name: policyid; Type: DEFAULT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY inspolicies ALTER COLUMN policyid SET DEFAULT nextval('inspolicies_policyid_seq'::regclass);


--
-- TOC entry 2008 (class 2604 OID 53501)
-- Name: insurancecoid; Type: DEFAULT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY insurancecos ALTER COLUMN insurancecoid SET DEFAULT nextval('insurancecos_insurancecoid_seq'::regclass);


--
-- TOC entry 2009 (class 2604 OID 53502)
-- Name: pharmacyid; Type: DEFAULT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY pharmacies ALTER COLUMN pharmacyid SET DEFAULT nextval('pharmacies_pharmacyid_seq'::regclass);


--
-- TOC entry 2010 (class 2604 OID 53503)
-- Name: practitionerid; Type: DEFAULT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY practitioners ALTER COLUMN practitionerid SET DEFAULT nextval('practitioners_practitionerid_seq'::regclass);


--
-- TOC entry 2011 (class 2604 OID 53504)
-- Name: practtypeid; Type: DEFAULT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY practtypes ALTER COLUMN practtypeid SET DEFAULT nextval('practtypes_practtypeid_seq'::regclass);


--
-- TOC entry 2012 (class 2604 OID 53505)
-- Name: chargeid; Type: DEFAULT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY prescriptions ALTER COLUMN chargeid SET DEFAULT nextval('charges_chargeid_seq'::regclass);


--
-- TOC entry 2013 (class 2604 OID 53506)
-- Name: chargeid; Type: DEFAULT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY profees ALTER COLUMN chargeid SET DEFAULT nextval('charges_chargeid_seq'::regclass);


--
-- TOC entry 2014 (class 2604 OID 53507)
-- Name: taxexptypeid; Type: DEFAULT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY taxexptypes ALTER COLUMN taxexptypeid SET DEFAULT nextval('taxexptypes_taxexptypeid_seq'::regclass);


--
-- TOC entry 2018 (class 2606 OID 53509)
-- Name: charges_pk; Type: CONSTRAINT; Schema: public; Owner: ptipper; Tablespace: 
--

ALTER TABLE ONLY charges
    ADD CONSTRAINT charges_pk PRIMARY KEY (chargeid);


--
-- TOC entry 2021 (class 2606 OID 53511)
-- Name: families_pk; Type: CONSTRAINT; Schema: public; Owner: ptipper; Tablespace: 
--

ALTER TABLE ONLY families
    ADD CONSTRAINT families_pk PRIMARY KEY (familyid);


--
-- TOC entry 2024 (class 2606 OID 53513)
-- Name: familymembers_pk; Type: CONSTRAINT; Schema: public; Owner: ptipper; Tablespace: 
--

ALTER TABLE ONLY familymembers
    ADD CONSTRAINT familymembers_pk PRIMARY KEY (memberid);


--
-- TOC entry 2028 (class 2606 OID 53515)
-- Name: insccrules_pk; Type: CONSTRAINT; Schema: public; Owner: ptipper; Tablespace: 
--

ALTER TABLE ONLY insccrules
    ADD CONSTRAINT insccrules_pk PRIMARY KEY (claimcodeid, policyid);


--
-- TOC entry 2030 (class 2606 OID 53517)
-- Name: insclaimcodes_pk; Type: CONSTRAINT; Schema: public; Owner: ptipper; Tablespace: 
--

ALTER TABLE ONLY insclaimcodes
    ADD CONSTRAINT insclaimcodes_pk PRIMARY KEY (claimcodeid);


--
-- TOC entry 2033 (class 2606 OID 53519)
-- Name: insclaims_pk; Type: CONSTRAINT; Schema: public; Owner: ptipper; Tablespace: 
--

ALTER TABLE ONLY insclaims
    ADD CONSTRAINT insclaims_pk PRIMARY KEY (claimid);


--
-- TOC entry 2036 (class 2606 OID 53521)
-- Name: inspolicies_pk; Type: CONSTRAINT; Schema: public; Owner: ptipper; Tablespace: 
--

ALTER TABLE ONLY inspolicies
    ADD CONSTRAINT inspolicies_pk PRIMARY KEY (policyid);


--
-- TOC entry 2038 (class 2606 OID 53523)
-- Name: insurancecos_primarykey; Type: CONSTRAINT; Schema: public; Owner: ptipper; Tablespace: 
--

ALTER TABLE ONLY insurancecos
    ADD CONSTRAINT insurancecos_primarykey PRIMARY KEY (insurancecoid);


--
-- TOC entry 2040 (class 2606 OID 53525)
-- Name: pharmacies_pk; Type: CONSTRAINT; Schema: public; Owner: ptipper; Tablespace: 
--

ALTER TABLE ONLY pharmacies
    ADD CONSTRAINT pharmacies_pk PRIMARY KEY (pharmacyid);


--
-- TOC entry 2043 (class 2606 OID 53527)
-- Name: practitioners_pk; Type: CONSTRAINT; Schema: public; Owner: ptipper; Tablespace: 
--

ALTER TABLE ONLY practitioners
    ADD CONSTRAINT practitioners_pk PRIMARY KEY (practitionerid);


--
-- TOC entry 2047 (class 2606 OID 53529)
-- Name: practtypes_pk; Type: CONSTRAINT; Schema: public; Owner: ptipper; Tablespace: 
--

ALTER TABLE ONLY practtypes
    ADD CONSTRAINT practtypes_pk PRIMARY KEY (practtypeid);


--
-- TOC entry 2051 (class 2606 OID 53531)
-- Name: prescriptions_pk; Type: CONSTRAINT; Schema: public; Owner: ptipper; Tablespace: 
--

ALTER TABLE ONLY prescriptions
    ADD CONSTRAINT prescriptions_pk PRIMARY KEY (chargeid);


--
-- TOC entry 2056 (class 2606 OID 53533)
-- Name: profees_pk; Type: CONSTRAINT; Schema: public; Owner: ptipper; Tablespace: 
--

ALTER TABLE ONLY profees
    ADD CONSTRAINT profees_pk PRIMARY KEY (chargeid);


--
-- TOC entry 2058 (class 2606 OID 53535)
-- Name: taxexptypes_primarykey; Type: CONSTRAINT; Schema: public; Owner: ptipper; Tablespace: 
--

ALTER TABLE ONLY taxexptypes
    ADD CONSTRAINT taxexptypes_primarykey PRIMARY KEY (taxexptypeid);


--
-- TOC entry 2062 (class 2606 OID 130546)
-- Name: tempfees_pkey; Type: CONSTRAINT; Schema: public; Owner: ptipper; Tablespace: 
--

ALTER TABLE ONLY tempfees
    ADD CONSTRAINT tempfees_pkey PRIMARY KEY (chargeid);


--
-- TOC entry 2019 (class 1259 OID 53538)
-- Name: fki_charges_fk_familymembers; Type: INDEX; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE INDEX fki_charges_fk_familymembers ON charges USING btree (familymemberid);


--
-- TOC entry 2022 (class 1259 OID 53539)
-- Name: fki_families_fk_inspolicies; Type: INDEX; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE INDEX fki_families_fk_inspolicies ON families USING btree (policyid);


--
-- TOC entry 2025 (class 1259 OID 53540)
-- Name: fki_familymembers_fk_families; Type: INDEX; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE INDEX fki_familymembers_fk_families ON familymembers USING btree (familyid);


--
-- TOC entry 2026 (class 1259 OID 53541)
-- Name: fki_insccrules_fk_inspolicies; Type: INDEX; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE INDEX fki_insccrules_fk_inspolicies ON insccrules USING btree (policyid);


--
-- TOC entry 2031 (class 1259 OID 53542)
-- Name: fki_insclaims_fk_inspolicies; Type: INDEX; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE INDEX fki_insclaims_fk_inspolicies ON insclaims USING btree (policyid);


--
-- TOC entry 2034 (class 1259 OID 53543)
-- Name: fki_inspolicies_fk_insurancecos; Type: INDEX; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE INDEX fki_inspolicies_fk_insurancecos ON inspolicies USING btree (insurancecoid);


--
-- TOC entry 2041 (class 1259 OID 53544)
-- Name: fki_practitioners_fk_practtypes; Type: INDEX; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE INDEX fki_practitioners_fk_practtypes ON practitioners USING btree (practtypeid);


--
-- TOC entry 2044 (class 1259 OID 53545)
-- Name: fki_practtypes_fk_insclaimcodes; Type: INDEX; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE INDEX fki_practtypes_fk_insclaimcodes ON practtypes USING btree (claimcodeid);


--
-- TOC entry 2045 (class 1259 OID 53546)
-- Name: fki_practtypes_taxexptypes; Type: INDEX; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE INDEX fki_practtypes_taxexptypes ON practtypes USING btree (taxexptypeid);


--
-- TOC entry 2048 (class 1259 OID 53547)
-- Name: fki_prescriptions_fk_familymembers; Type: INDEX; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE INDEX fki_prescriptions_fk_familymembers ON prescriptions USING btree (familymemberid);


--
-- TOC entry 2049 (class 1259 OID 53548)
-- Name: fki_prescriptions_fk_pharmacies; Type: INDEX; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE INDEX fki_prescriptions_fk_pharmacies ON prescriptions USING btree (pharmacyid);


--
-- TOC entry 2052 (class 1259 OID 53549)
-- Name: fki_profees_fk_familymembers; Type: INDEX; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE INDEX fki_profees_fk_familymembers ON profees USING btree (familymemberid);


--
-- TOC entry 2053 (class 1259 OID 53550)
-- Name: fki_profees_fk_insclaims; Type: INDEX; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE INDEX fki_profees_fk_insclaims ON profees USING btree (insclaimid);


--
-- TOC entry 2054 (class 1259 OID 53551)
-- Name: fki_profees_fk_practitioners; Type: INDEX; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE INDEX fki_profees_fk_practitioners ON profees USING btree (practitionerid);


--
-- TOC entry 2059 (class 1259 OID 130562)
-- Name: tempfees_familymemberid_idx; Type: INDEX; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE INDEX tempfees_familymemberid_idx ON tempfees USING btree (familymemberid);


--
-- TOC entry 2060 (class 1259 OID 130563)
-- Name: tempfees_insclaimid_idx; Type: INDEX; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE INDEX tempfees_insclaimid_idx ON tempfees USING btree (insclaimid);


--
-- TOC entry 2063 (class 1259 OID 130564)
-- Name: tempfees_practitionerid_idx; Type: INDEX; Schema: public; Owner: ptipper; Tablespace: 
--

CREATE INDEX tempfees_practitionerid_idx ON tempfees USING btree (practitionerid);


--
-- TOC entry 2064 (class 2606 OID 53555)
-- Name: charges_fk_familymembers; Type: FK CONSTRAINT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY charges
    ADD CONSTRAINT charges_fk_familymembers FOREIGN KEY (familymemberid) REFERENCES familymembers(memberid);


--
-- TOC entry 2065 (class 2606 OID 53560)
-- Name: families_fk_inspolicies; Type: FK CONSTRAINT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY families
    ADD CONSTRAINT families_fk_inspolicies FOREIGN KEY (policyid) REFERENCES inspolicies(policyid);


--
-- TOC entry 2066 (class 2606 OID 53565)
-- Name: familymembers_fk_families; Type: FK CONSTRAINT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY familymembers
    ADD CONSTRAINT familymembers_fk_families FOREIGN KEY (familyid) REFERENCES families(familyid);


--
-- TOC entry 2067 (class 2606 OID 53570)
-- Name: insccrules_fk_insclaimcodes; Type: FK CONSTRAINT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY insccrules
    ADD CONSTRAINT insccrules_fk_insclaimcodes FOREIGN KEY (claimcodeid) REFERENCES insclaimcodes(claimcodeid);


--
-- TOC entry 2068 (class 2606 OID 53575)
-- Name: insccrules_fk_inspolicies; Type: FK CONSTRAINT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY insccrules
    ADD CONSTRAINT insccrules_fk_inspolicies FOREIGN KEY (policyid) REFERENCES inspolicies(policyid);


--
-- TOC entry 2069 (class 2606 OID 53580)
-- Name: insclaims_fk_inspolicies; Type: FK CONSTRAINT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY insclaims
    ADD CONSTRAINT insclaims_fk_inspolicies FOREIGN KEY (policyid) REFERENCES inspolicies(policyid);


--
-- TOC entry 2070 (class 2606 OID 53585)
-- Name: inspolicies_fk_insurancecos; Type: FK CONSTRAINT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY inspolicies
    ADD CONSTRAINT inspolicies_fk_insurancecos FOREIGN KEY (insurancecoid) REFERENCES insurancecos(insurancecoid);


--
-- TOC entry 2071 (class 2606 OID 53590)
-- Name: practitioners_fk_practtypes; Type: FK CONSTRAINT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY practitioners
    ADD CONSTRAINT practitioners_fk_practtypes FOREIGN KEY (practtypeid) REFERENCES practtypes(practtypeid);


--
-- TOC entry 2072 (class 2606 OID 53595)
-- Name: practtypes_fk_insclaimcodes; Type: FK CONSTRAINT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY practtypes
    ADD CONSTRAINT practtypes_fk_insclaimcodes FOREIGN KEY (claimcodeid) REFERENCES insclaimcodes(claimcodeid);


--
-- TOC entry 2073 (class 2606 OID 53600)
-- Name: practtypes_fk_taxexptypes; Type: FK CONSTRAINT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY practtypes
    ADD CONSTRAINT practtypes_fk_taxexptypes FOREIGN KEY (taxexptypeid) REFERENCES taxexptypes(taxexptypeid);


--
-- TOC entry 2074 (class 2606 OID 53605)
-- Name: prescriptions_fk_familymembers; Type: FK CONSTRAINT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY prescriptions
    ADD CONSTRAINT prescriptions_fk_familymembers FOREIGN KEY (familymemberid) REFERENCES familymembers(memberid);


--
-- TOC entry 2075 (class 2606 OID 53610)
-- Name: prescriptions_fk_pharmacies; Type: FK CONSTRAINT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY prescriptions
    ADD CONSTRAINT prescriptions_fk_pharmacies FOREIGN KEY (pharmacyid) REFERENCES pharmacies(pharmacyid);


--
-- TOC entry 2076 (class 2606 OID 53615)
-- Name: profees_fk_familymembers; Type: FK CONSTRAINT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY profees
    ADD CONSTRAINT profees_fk_familymembers FOREIGN KEY (familymemberid) REFERENCES familymembers(memberid);


--
-- TOC entry 2077 (class 2606 OID 53620)
-- Name: profees_fk_insclaims; Type: FK CONSTRAINT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY profees
    ADD CONSTRAINT profees_fk_insclaims FOREIGN KEY (insclaimid) REFERENCES insclaims(claimid);


--
-- TOC entry 2078 (class 2606 OID 53625)
-- Name: profees_fk_practitioners; Type: FK CONSTRAINT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY profees
    ADD CONSTRAINT profees_fk_practitioners FOREIGN KEY (practitionerid) REFERENCES practitioners(practitionerid);


--
-- TOC entry 2079 (class 2606 OID 130547)
-- Name: tempfees_fk_familymembers; Type: FK CONSTRAINT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY tempfees
    ADD CONSTRAINT tempfees_fk_familymembers FOREIGN KEY (familymemberid) REFERENCES familymembers(memberid);


--
-- TOC entry 2080 (class 2606 OID 130552)
-- Name: tempfees_fk_insclaims; Type: FK CONSTRAINT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY tempfees
    ADD CONSTRAINT tempfees_fk_insclaims FOREIGN KEY (insclaimid) REFERENCES insclaims(claimid);


--
-- TOC entry 2081 (class 2606 OID 130557)
-- Name: tempfees_fk_practitioners; Type: FK CONSTRAINT; Schema: public; Owner: ptipper
--

ALTER TABLE ONLY tempfees
    ADD CONSTRAINT tempfees_fk_practitioners FOREIGN KEY (practitionerid) REFERENCES practitioners(practitionerid);


--
-- TOC entry 2089 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-04-02 14:48:34

--
-- PostgreSQL database dump complete
--

