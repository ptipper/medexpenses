--
-- PostgreSQL database dump
--

-- Dumped from database version 9.2.2
-- Dumped by pg_dump version 9.2.2
-- Started on 2016-01-17 15:10:51

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2094 (class 0 OID 53451)
-- Dependencies: 181
-- Data for Name: insurancecos; Type: TABLE DATA; Schema: public; Owner: ptipper
--

COPY insurancecos (insurancecoid, name) FROM stdin;
1	VHI
\.


--
-- TOC entry 2092 (class 0 OID 53446)
-- Dependencies: 179
-- Data for Name: inspolicies; Type: TABLE DATA; Schema: public; Owner: ptipper
--

COPY inspolicies (policyid, insurancecoid, policyno, description) FROM stdin;
1	1	960615              	Company Plan Executive
\.


--
-- TOC entry 2083 (class 0 OID 53423)
-- Dependencies: 170
-- Data for Name: families; Type: TABLE DATA; Schema: public; Owner: ptipper
--

COPY families (familyid, familyname, policyid) FROM stdin;
1	Paul, Aine, Kevin and Neil Tipper	1
\.


--
-- TOC entry 2085 (class 0 OID 53428)
-- Dependencies: 172
-- Data for Name: familymembers; Type: TABLE DATA; Schema: public; Owner: ptipper
--

COPY familymembers (memberid, familyid, name) FROM stdin;
1	1	Paul
2	1	Aine
3	1	Kevin
4	1	Neil
\.


--
-- TOC entry 2081 (class 0 OID 53415)
-- Dependencies: 168
-- Data for Name: charges; Type: TABLE DATA; Schema: public; Owner: ptipper
--

COPY charges (chargeid, familymemberid, chargedate, chargevalue, notes, receiptimage, taxclaimstatus, inspayment, taxrelief) FROM stdin;
\.


--
-- TOC entry 2111 (class 0 OID 0)
-- Dependencies: 169
-- Name: charges_chargeid_seq; Type: SEQUENCE SET; Schema: public; Owner: ptipper
--

SELECT pg_catalog.setval('charges_chargeid_seq', 1, false);


--
-- TOC entry 2112 (class 0 OID 0)
-- Dependencies: 171
-- Name: families_familyid_seq; Type: SEQUENCE SET; Schema: public; Owner: ptipper
--

SELECT pg_catalog.setval('families_familyid_seq', 1, true);


--
-- TOC entry 2113 (class 0 OID 0)
-- Dependencies: 173
-- Name: familymembers_memberid_seq; Type: SEQUENCE SET; Schema: public; Owner: ptipper
--

SELECT pg_catalog.setval('familymembers_memberid_seq', 4, true);


--
-- TOC entry 2088 (class 0 OID 53436)
-- Dependencies: 175
-- Data for Name: insclaimcodes; Type: TABLE DATA; Schema: public; Owner: ptipper
--

COPY insclaimcodes (claimcodeid, insurancecoid, claimcode, description, maxvisits, maxclaimvalue, percentcover) FROM stdin;
1	1	3D  	Out-patient mental health treatment and assessment	1	€100.00	1.00
2	1	9A  	General practitioner visits	12	€40.00	1.00
3	1	9B  	Consultant consultations	12	€100.00	1.00
4	1	9D  	Radiology - consultants fees for prefessional services	999	€100.00	1.00
5	1	9E  	Pathology/radiology or other diagnostics tests	999	€1,000.00	0.75
6	1	9G  	Dental practitioner visits	12	€40.00	1.00
7	1	9H  	Physiotherapist visits	12	€40.00	1.00
8	1	9I  	Occupational therapists, Speech therapists	12	€40.00	1.00
9	1	9O  	Clinical psychologists	12	€40.00	1.00
10	1	9Q  	Child counselling	8	€30.00	1.00
\.


--
-- TOC entry 2087 (class 0 OID 53433)
-- Dependencies: 174
-- Data for Name: insccrules; Type: TABLE DATA; Schema: public; Owner: ptipper
--

COPY insccrules (claimcodeid, policyid, maxvisits, maxclaimvalue, percentcover) FROM stdin;
1	1	1	100.00	1.00
2	1	12	40.00	1.00
3	1	12	100.00	1.00
4	1	999	100.00	1.00
5	1	999	1000.00	0.75
6	1	12	40.00	1.00
7	1	12	40.00	1.00
8	1	12	40.00	1.00
9	1	12	40.00	1.00
10	1	8	30.00	1.00
\.


--
-- TOC entry 2114 (class 0 OID 0)
-- Dependencies: 176
-- Name: insclaimcodes_claimcodeid_seq; Type: SEQUENCE SET; Schema: public; Owner: ptipper
--

SELECT pg_catalog.setval('insclaimcodes_claimcodeid_seq', 1, false);


--
-- TOC entry 2090 (class 0 OID 53441)
-- Dependencies: 177
-- Data for Name: insclaims; Type: TABLE DATA; Schema: public; Owner: ptipper
--

COPY insclaims (claimid, claimref, claimdate, startdate, enddate, settled, policyid) FROM stdin;
\.


--
-- TOC entry 2115 (class 0 OID 0)
-- Dependencies: 178
-- Name: insclaims_claimid_seq; Type: SEQUENCE SET; Schema: public; Owner: ptipper
--

SELECT pg_catalog.setval('insclaims_claimid_seq', 1, false);


--
-- TOC entry 2116 (class 0 OID 0)
-- Dependencies: 180
-- Name: inspolicies_policyid_seq; Type: SEQUENCE SET; Schema: public; Owner: ptipper
--

SELECT pg_catalog.setval('inspolicies_policyid_seq', 1, true);


--
-- TOC entry 2117 (class 0 OID 0)
-- Dependencies: 182
-- Name: insurancecos_insurancecoid_seq; Type: SEQUENCE SET; Schema: public; Owner: ptipper
--

SELECT pg_catalog.setval('insurancecos_insurancecoid_seq', 1, true);


--
-- TOC entry 2096 (class 0 OID 53456)
-- Dependencies: 183
-- Data for Name: pharmacies; Type: TABLE DATA; Schema: public; Owner: ptipper
--

COPY pharmacies (pharmacyid, name) FROM stdin;
1	L.B. Bhagwan
2	O'Connell's Pharmacy
3	P.M.K Pharmacy Ltd.
4	Ryall's Pharmacy
5	Rafferty Chemists
6	Macken's Pharmacy
7	Unicare Bates Pharmacy
8	Tony Walsh Rathfarnham
9	Bradley's Pharmacy
10	Kilmacud Pharmacy
11	Slievemore Pharmacy
12	Fahy's Pharmacy
14	Blackrock Clinic Pharmacy
15	Rockfield Pharmacy
16	McCabe's Pharmacy
17	DocMorris Pharmacy
18	Lloyds Pharmacy
19	Haven Pharmacy
\.


--
-- TOC entry 2118 (class 0 OID 0)
-- Dependencies: 184
-- Name: pharmacies_pharmacyid_seq; Type: SEQUENCE SET; Schema: public; Owner: ptipper
--

SELECT pg_catalog.setval('pharmacies_pharmacyid_seq', 1, false);


--
-- TOC entry 2104 (class 0 OID 53483)
-- Dependencies: 191
-- Data for Name: taxexptypes; Type: TABLE DATA; Schema: public; Owner: ptipper
--

COPY taxexptypes (taxexptypeid, description, category, section) FROM stdin;
1	Maintenance or treatment in an approved nursing home	\N	1
2	Non-Routine Dental Treatment (per Med 2)	\N	2
3	Services of a doctor/consultant	a         	3
4	Total outlay on prescribed drugs/medicines for the year	b         	3
5	Educational Psychological Assessment for a dependent child	c         	3
6	Speech and Language Therapy for a dependent child	d         	3
7	Orthoptic or similar treatment	e         	3
8	Diagnostic procedures (X-rays, etc.)	f         	3
9	Physiotherapy or similar treatment	g         	3
10	Expenses incurred on any medical, surgical or nursing appliance	h         	3
11	Maintenance or treatment in a hospital	i         	3
12	Other Qualifying Expenses	j         	3
13	Not eligible for tax relief	\N	0
\.


--
-- TOC entry 2100 (class 0 OID 53466)
-- Dependencies: 187
-- Data for Name: practtypes; Type: TABLE DATA; Schema: public; Owner: ptipper
--

COPY practtypes (practtypeid, description, claimcodeid, taxexptypeid) FROM stdin;
1	General practitioner	2	3
2	Consultant	3	3
3	Physiotherapist	7	9
4	Psychiatrist	1	5
5	Hospital	\N	11
6	Psychotherapist	9	5
7	Occupational Therapist	8	6
8	Speech and language therapist	8	6
9	Dentist	6	2
10	Clinical Psychologist	9	5
11	Nurse	2	3
12	Consultant radiologist	4	8
13	Radiologist (technical services	5	8
14	Dietitian	8	3
15	Child counsellor	10	13
\.


--
-- TOC entry 2098 (class 0 OID 53461)
-- Dependencies: 185
-- Data for Name: practitioners; Type: TABLE DATA; Schema: public; Owner: ptipper
--

COPY practitioners (practitionerid, title, firstname, surname, practtypeid) FROM stdin;
1	Dr.	Anthony	Ryan	2
2	\N	\N	Coombe Hospital	5
3	Dr.	Gertrude	Ronan	1
4	\N	\N	Glencairn Medical Centre	5
5	Dr.	Maura	Slevin	1
6	Dr.	\N	O'Connell	1
7	\N	\N	Our Lady's Hospital, Crumlin	5
8	Dr.	Rodney	Regan	1
9	Dr.	\N	Sheehy	1
10	Dr.	Tiernan	Murray	1
11	\N	\N	Unknown	\N
12	Mr.	John D.	Russell	2
13	Dr.	David	Carey	4
14	Prof.	Michael	Fitzgerald	4
15	\N	\N	Blackrock Clinic	5
16	Dr.	John	O'Keefe	2
17	Dr.	Harry	Beauchamp	2
19	Dr.	Thomas	Grimm	1
20	Dr.	Conor	O'Toole	1
21	Dr.	Paul	Carson	1
22	Dr.	Ciara	McMahon	1
23	Ms.	Catherine	Murtagh	6
24	Ms.	Pauline	Moran	7
25	Ms.	Catherine	Milford	7
26	\N	\N	Milltown Physiotherapy Clinic	3
27	Ms.	Tara	Curran	3
28	Dr.	Conor	O'Brien	2
29	Dr.	Michael	O'Brien	1
30	Dr.	Richard	Casey	1
31	\N	Blackrock	Clinic	3
32	Dr.	\N	Palmer	1
33	Dr.	William F.	Hooper	1
34	Dr.	\N	Sheehy	1
35	Dr.	\N	Ali	1
36	Ms.	Mary-Rose	Harkin	6
37	Ms.	Aisling	de Brit	4
38	Ms.	Marinet	van Vuren	8
39	Ms.	Deirdre	Fahey	8
40	Dr.	Rita	Honan	10
41	Mr.	Barry	Devaney	9
42	Ms.	Kiera	Brown	3
43	Prof.	Prem	Puri	2
44	Dr.	Lisa	Fay	1
45	Ms.	Lita	Griffin	11
46	Dr.	Karen	Palmer	1
47	Dr.	Jane	McDonagh	1
48	Mr.	Ciaran	Lanagan	1
49	\N	VHI	Swiftcare	1
50	Dr.	Nicholas	Walsh	2
51	Ms.	Joanne	Barrett	3
52	Dr.	Georgina	Connellan	1
53	\N	Fay	Cosmetics	2
54	Dr.	Shafeeq	Alraqi	2
55	Dr.	Debbie	Fitzgerald	1
56	Dr.	Jennie	Scott	1
57	Ms.	Lisa	Keogh	11
58	\N	St.	Vincents Hospital	5
59	\N	\N	Blackrock Clinic Radiography (professional services)	12
60	\N	\N	Blackrock Clinic Radiography (technical services)	13
61	\N	\N	St. James Hospital	5
62	\N	\N	UPMC Beacon Hospital	5
63	Dr.	Sharon	Moss	2
64	Dr.	Geraldine	Morrow	2
65	Prof.	Thomas	Lynch	2
66	Dr.	Hairi	Lasim	1
67	Dr.	Ciara	Keating	1
68	Prof.	Mark	Redmond	2
69	Mr.	William J.	Power	2
70	Dr.	Martin	Quinn	2
71	Dr.	Gillian	McLaughlin	1
72	Dr.	Aisling	Brady	1
73	Ms.	Jean	Kenny	10
74	Ms.	Gillian	McConnell	14
76	Dr.	John A	Hogan	9
77	Mr.	Michael	Ryan	15
\.


--
-- TOC entry 2119 (class 0 OID 0)
-- Dependencies: 186
-- Name: practitioners_practitionerid_seq; Type: SEQUENCE SET; Schema: public; Owner: ptipper
--

SELECT pg_catalog.setval('practitioners_practitionerid_seq', 1, false);


--
-- TOC entry 2120 (class 0 OID 0)
-- Dependencies: 188
-- Name: practtypes_practtypeid_seq; Type: SEQUENCE SET; Schema: public; Owner: ptipper
--

SELECT pg_catalog.setval('practtypes_practtypeid_seq', 1, false);


--
-- TOC entry 2102 (class 0 OID 53471)
-- Dependencies: 189
-- Data for Name: prescriptions; Type: TABLE DATA; Schema: public; Owner: ptipper
--

COPY prescriptions (chargeid, familymemberid, chargedate, chargevalue, notes, receiptimage, taxclaimstatus, inspayment, taxrelief, pharmacyid, formno) FROM stdin;
\.


--
-- TOC entry 2103 (class 0 OID 53477)
-- Dependencies: 190
-- Data for Name: profees; Type: TABLE DATA; Schema: public; Owner: ptipper
--

COPY profees (chargeid, familymemberid, chargedate, chargevalue, notes, receiptimage, taxclaimstatus, inspayment, taxrelief, practitionerid, insclaimid, claimable, insclaimstatus) FROM stdin;
\.


--
-- TOC entry 2121 (class 0 OID 0)
-- Dependencies: 192
-- Name: taxexptypes_taxexptypeid_seq; Type: SEQUENCE SET; Schema: public; Owner: ptipper
--

SELECT pg_catalog.setval('taxexptypes_taxexptypeid_seq', 1, false);


--
-- TOC entry 2106 (class 0 OID 53488)
-- Dependencies: 193
-- Data for Name: tempfees; Type: TABLE DATA; Schema: public; Owner: ptipper
--

COPY tempfees (chargeid, familymemberid, chargedate, chargevalue, notes, receiptimage, taxclaimstatus, inspayment, taxrelief, practitionerid, insclaimid, claimable, insclaimstatus) FROM stdin;
\.


-- Completed on 2016-01-17 15:10:51

--
-- PostgreSQL database dump complete
--

