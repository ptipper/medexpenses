<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Prescriptions;

/**
 * PrescriptionsSearch represents the model behind the search form about `app\models\Prescriptions`.
 */
class PrescriptionsSearch extends Prescriptions
{
    /**
     * @inheritdoc
     */
    
    public $familymember;      // Family model object
    public $pharmacy;          // Pharmacy model object
	
    public function rules()
    {
        return [
            [['chargeid', 'familymemberid', 'pharmacyid'], 'integer'],
            [['chargedate', 'notes', 'receiptimage', 'taxclaimstatus', 'formno','familymember','pharmacy'], 'safe'],
            [['chargevalue', 'inspayment', 'taxrelief'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Prescriptions::find();
        $query->joinWith('familymember', true, 'INNER JOIN');
		$query->joinWith('pharmacy', true, 'INNER JOIN');
		$query->orderBy(['chargeid' => SORT_DESC]);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        // Allow sorting by related columns
		$dataProvider->sort->attributes['familymember'] = [
		    // The tables are the ones our relation are configured to
		    'asc' => ['familymembers.name' => SORT_ASC],
		    'desc' => ['familymembers.name' => SORT_DESC],
		];

		// Allow sorting by related columns
		$dataProvider->sort->attributes['pharmacy'] = [
		    // The tables are the ones our relation are configured to
		    'asc' => ['pharmacies.name' => SORT_ASC],
		    'desc' => ['pharmacies.name' => SORT_DESC],
		];

		$this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'chargeid' => $this->chargeid,
            'familymemberid' => $this->familymemberid,
            'chargedate' => $this->chargedate,
            'chargevalue' => $this->chargevalue,
            'inspayment' => $this->inspayment,
            'taxrelief' => $this->taxrelief,
            'pharmacyid' => $this->pharmacyid,
        ]);

        $query->andFilterWhere(['like', 'notes', $this->notes])
            ->andFilterWhere(['like', 'receiptimage', $this->receiptimage])
            ->andFilterWhere(['like', 'taxclaimstatus', $this->taxclaimstatus])
            ->andFilterWhere(['like', 'formno', $this->formno])
            ->andFilterWhere(['like', 'familymembers.name', $this->familymember])
		    ->andFilterWhere(['like', 'pharmacies.name', $this->pharmacy])
		    ;

        return $dataProvider;
    }
}
