<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "insclaimcodes".
 *
 * @property integer $claimcodeid
 * @property integer $insurancecoid
 * @property string $claimcode
 * @property string $description
 * @property integer $maxvisits
 * @property string $maxclaimvalue
 * @property string $percentcover
 *
 * @property Practtypes[] $practtypes
 */
class Insclaimcodes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'insclaimcodes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['insurancecoid', 'maxvisits'], 'integer'],
            [['maxclaimvalue', 'percentcover'], 'number'],
            [['claimcode'], 'string', 'max' => 4],
            [['description'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'claimcodeid' => 'Claim code id',
            'insurancecoid' => 'Insurance company id',
            'claimcode' => 'Insurance claim code',
            'description' => 'Description',
            'maxvisits' => 'Maximum number of visits per claim',
            'maxclaimvalue' => 'Maximum claim value',
            'percentcover' => 'Percentage cover',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPracttypes()
    {
        return $this->hasMany(Practtypes::className(), ['claimcodeid' => 'claimcodeid']);
    }
}
