<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Taxexptypes;

/**
 * TaxexptypesSearch represents the model behind the search form about Taxexptypes.
 */
class TaxexptypesSearch extends Model
{
	public $taxexptypeid;
	public $description;
	public $section;
	public $category;

	public function rules()
	{
		return [
			[['taxexptypeid'], 'integer'],
			[['description', 'section', 'category'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'taxexptypeid' => 'Tax expense type id',
			'description' => 'Description',
			'section' => 'Section code',
			'category' => 'Category code',
		];
	}

	public function search($params)
	{
		$query = Taxexptypes::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'taxexptypeid' => $this->taxexptypeid,
        ]);

		$query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'section', $this->section])
            ->andFilterWhere(['like', 'category', $this->category]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
