<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Insclaims;

/**
 * InsclaimsSearch represents the model behind the search form about `app\models\Insclaims`.
 */
class InsclaimsSearch extends Insclaims
{
    /**
     * @inheritdoc
     */
    
    public $policy;    // Insurance policies relation
    
    public function rules()
    {
        return [
            [['claimid', 'policyid'], 'integer'],
            [['claimref', 'claimdate', 'startdate', 'enddate','policy'], 'safe'],
            [['settled'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Insclaims::find();
        $query->joinWith('policy', true, 'INNER JOIN');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // Allow sorting by related columns
		$dataProvider->sort->attributes['policy'] = [
		    // The tables are the ones our relation are configured to
		    'asc' => ['inspolicies.policyno' => SORT_ASC, 'inspolicies.description' => SORT_ASC],
		    'desc' => ['inspolicies.policyno' => SORT_DESC, 'inspolicies.description' => SORT_DESC],
		];

		$this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'claimid' => $this->claimid,
            'claimdate' => $this->claimdate,
            'startdate' => $this->startdate,
            'enddate' => $this->enddate,
            'settled' => $this->settled,
            'policyid' => $this->policyid,
        ]);

        $query->andFilterWhere(['like', 'claimref', $this->claimref])
              ->andFilterWhere(['like', 'inspolicies.policyno', $this->policy])
              ->orFilterWhere(['like', 'inspolicies.description', $this->policy]);

        return $dataProvider;
    }
}
