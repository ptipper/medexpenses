<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Inspolicies;

/**
 * InspoliciesSearch represents the model behind the search form about Inspolicies.
 */
class InspoliciesSearch extends Model
{
	public $policyid;
	public $insuranceco;
	public $insurancecoid;
	public $policyno;
	public $description;

	public function rules()
	{
		return [
			[['policyid', 'insurancecoid'], 'integer'],
			[['policyno', 'description', 'insuranceco'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'policyid' => 'Insurance policy id',
			'insurancecoid' => 'Insurance company id',
			'policyno' => 'Policy number',
			'description' => 'Policy description',
		];
	}

	public function search($params)
	{
		$query = Inspolicies::find()->joinWith('insuranceco', true, 'INNER JOIN');
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		// Important: here is how we set up the sorting
        // The key is the attribute name on our "InspoliciesSearch" instance
        $dataProvider->sort->attributes['insuranceco'] = [
            // The tables are the ones our relation are configured to
            'asc' => ['insurancecos.name' => SORT_ASC],
            'desc' => ['insurancecos.name' => SORT_DESC],
        ];
    
    if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'policyid' => $this->policyid,
            'insurancecoid' => $this->insurancecoid,
        ]);

		$query->andFilterWhere(['like', 'policyno', $this->policyno])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'insurancecos.name', $this->insuranceco])
		;

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
