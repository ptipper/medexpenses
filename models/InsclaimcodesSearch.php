<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Insclaimcodes;

/**
 * InsclaimcodesSearch represents the model behind the search form about `app\models\Insclaimcodes`.
 */
class InsclaimcodesSearch extends Insclaimcodes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['claimcodeid', 'insurancecoid', 'maxvisits'], 'integer'],
            [['claimcode', 'description'], 'safe'],
            [['maxclaimvalue', 'percentcover'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Insclaimcodes::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'claimcodeid' => $this->claimcodeid,
            'insurancecoid' => $this->insurancecoid,
            'maxvisits' => $this->maxvisits,
            'maxclaimvalue' => $this->maxclaimvalue,
            'percentcover' => $this->percentcover,
        ]);

        $query->andFilterWhere(['like', 'claimcode', $this->claimcode])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
