<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Practtypes;

/**
 * PracttypesSearch represents the model behind the search form about Practtypes.
 */
class PracttypesSearch extends Model
{
	public $practtypeid;
	public $description;
	public $claimcodeid;
	public $claimcode;
	public $taxexptypeid;
	public $taxexptype;
	
	public function rules()
	{
		return [
			[['practtypeid', 'claimcodeid', 'taxexptypeid'], 'integer'],
			[['description', 'claimcode','taxexptype'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'practtypeid' => 'Practitioner type id',
			'description' => 'Description',
			'claimcodeid' => 'Insurance claim code id',
			'taxexptypeid' => 'Tax expense type id',
		];
	}

	public function search($params)
	{
		$query = Practtypes::find();
		$query->joinWith('claimcode', true, 'LEFT JOIN');
		$query->joinWith('taxexptype', true, 'LEFT JOIN');
		
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		// Allow sorting by insurance claim code description
		$dataProvider->sort->attributes['claimcode'] = [
		    // The tables are the ones our relation are configured to
		    'asc' => ['insclaimcodes.description' => SORT_ASC],
		    'desc' => ['insclaimcodes.description' => SORT_DESC],
		];
		
		// Allow sorting by insurance claim code description
		$dataProvider->sort->attributes['taxexptype'] = [
		    // The tables are the ones our relation are configured to
		    'asc' => ['taxexptypes.description' => SORT_ASC],
		    'desc' => ['taxexptypes.description' => SORT_DESC],
		];
		
		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'practtypeid' => $this->practtypeid,
            'claimcodeid' => $this->claimcodeid,
            'taxexptypeid' => $this->taxexptypeid,
        ]);

		$query->andFilterWhere(['like', 'description', $this->description])
		      ->andFilterWhere(['like', 'insclaimcodes.description', $this->claimcode])
		      ->andFilterWhere(['like', 'taxexptypes.description', $this->taxexptype]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
