<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use app\models\Profees;
use app\models\Tempfees;
use app\models\ChargeSummaryItem;
use app\models\Insccrules;
use yii\db\Query;
use yii\data\yii\data;
use \DateTime;

/**
 * This is the model class for table "insclaims".
 */
class Insclaims extends \app\models\base\Insclaims
{
    /**
     * 
     * @var ArrayDataProvider Charges data provider
     */
    public $chargesProvider;
    
    /**
     * Load charges into the charges array
    * @param boolean $initTempFees If true, then initialise the temporary fees table and populate it with charges associated with this claim
      */
    public function loadCharges($initTempFees = false)
    {
        if ($initTempFees) {
            $this->populateTempFeesTable();
            
        }
        
        // Create the data provider for the charges associated with this claim
        $query = $this->getTempfees();
        
        $this->chargesProvider =  new ActiveDataProvider([
            'query' => $query->orderBy('chargeid ASC'),
            'pagination' => [
                'pageSize' => 6,
            ],
            'sort' => [
                'attributes' => [
                    'chargeid', 
                    'familymember' => [
            		    'asc' => ['familymember.name' => SORT_ASC],
            		    'desc' => ['familymember.name' => SORT_DESC],
            		], 
                    'practitioner' => [
            		    'asc' => ['practitioner.surname' => SORT_ASC],
            		    'desc' => ['practitioner.surname' => SORT_DESC],
            		],
                    'chargedate',
                    'chargevalue',
                    'notes',
                ],
            ],
            
        ]);
        
    }
    
    /**
     * Populate the temporary fees table with fees linked to this insurance claim
     */
    private function populateTempFeesTable()
    {
        // Delete all entries in the temporary fees table
        Tempfees::deleteAll();
        
        if ($this->claimid != null) {
            // Copy the fees linked to this claim into the temporary fees table
            Yii::$app->db
            ->createCommand('INSERT INTO tempfees SELECT * FROM profees WHERE profees.insclaimid = ' . $this->claimid)
            ->execute();
        }
    }
    
    /**
     * Import eligible medical charges into this claim
     * @param string $startDate
     * @param string $endDate
     * @return integer Number of charge items imported
     */
    public function importCharges($startDate, $endDate)
    {
        $chargeSummaryItems = $this->buildChargeSummary($startDate, $endDate);
        $chargesImported = 0;
        
        $query = Profees::find()
                    ->joinWith('familymember', true, 'INNER JOIN')
                    ->joinWith('practitioner', true, 'INNER JOIN')
                    ->joinWith('practitioner.practtype', true, 'INNER JOIN')
                    ->joinWith('practitioner.practtype.claimcode', true, 'INNER JOIN')
                    ->where(['or',['profees.insclaimid' => 0, 'profees.insclaimid' => null]])
                    ->andWhere(['profees.insclaimstatus' => 'Not claimed'])
                    ->andWhere(['profees.claimable' => true])
                    ->andWhere(['>=','profees.chargedate',$startDate])
                    ->andWhere(['<=','profees.chargedate',$endDate])
                    ->orderBy('profees.familymemberid, insclaimcodes.claimcodeid')
                    ->indexBy('chargeid')
        ;
        
        $charges = $query->all();
        
        foreach ($charges as $profee) {
            
            $chargeid = $profee->chargeid;
            
            // Has this charge already been imported?
            if (Tempfees::findOne($chargeid)) {
                // It has, so skip this charge
                continue;
            }
            
            // Lookup the family member/claim code combination in the charge summary items array
            $familymemberid = $profee->familymemberid;
            $claimcodeid = $profee->practitioner->practtype->claimcodeid;
            
            if (isset($chargeSummaryItems[$familymemberid][$claimcodeid])) {
                
                $chargeSummaryItem = $chargeSummaryItems[$familymemberid][$claimcodeid];
                
            } else {
                // We haven't seen family member/claim code combination before, so add it to the charge summary items array
                $chargeSummaryItem = new ChargeSummaryItem();
                
                $chargeSummaryItem->familyMemberId = $familymemberid;
                $chargeSummaryItem->claimCodeId = $claimcodeid;
                
                // Look up the insurance rule for this claim code
                $insccrule = Insccrules::findOne(['claimcodeid' => $claimcodeid, 'policyid' => $this->policyid]);
                
                if ($insccrule != null) {
                
                    $chargeSummaryItem->maxVisits = $insccrule->maxvisits;
                    $chargeSummaryItem->maxClaimableValue = $insccrule->maxclaimvalue;
                    $chargeSummaryItem->percentCover = $insccrule->percentcover;
                    
                }
                
                $chargeSummaryItems[$familymemberid][$claimcodeid] = $chargeSummaryItem;
            }
            
            // Check that this charge can be claimed without breaching the terms of the claim rule
            // If any of the rules would be breached, then skip this item
            if ($chargeSummaryItem->visits + 1 > $chargeSummaryItem->maxVisits) {
                continue;
            }
            
            $claimableValue = $profee->chargevalue * $chargeSummaryItem->percentCover;
			if ($chargeSummaryItem->maxClaimableValue > 0.0) {
				$claimableValue = min($claimableValue, $chargeSummaryItem->maxClaimableValue);
			}
			
			if ($chargeSummaryItem->maxPayablePerAnnum > 0.0 && $chargeSummaryItem->totalValue + $claimableValue > $chargeSummaryItem->maxPayablePerAnnum) {
				continue;
			}
            
            $chargeSummaryItem->visits++;
            $chargeSummaryItem->totalValue += $claimableValue;
            
            // Save the professional fee to the temporary table
            $tempfee = new Tempfees();
            $tempfee->insclaimid = $this->claimid;
            $tempfee->chargeid = $profee->chargeid;
            $tempfee->familymemberid = $profee->familymemberid;
            $tempfee->chargedate = $profee->chargedate;
            $tempfee->chargevalue = $profee->chargevalue;
            $tempfee->notes = $profee->notes;
            $tempfee->receiptimage = $profee->receiptimage;
            $tempfee->practitionerid = $profee->practitionerid;
            $tempfee->claimable = $profee->claimable;
			$tempfee->inspayment = $claimableValue;
            $tempfee->insclaimstatus = $profee->insclaimstatus;
            
            if (!$tempfee->save()) {
                // Error inserting row in temporary fees table
                throw new \Exception('Error inserting row in temporary fees table.');
            }
            
            $chargesImported++;
            
        }
        
        return $chargesImported;
        
    }
    
    /**
     * Build an array of ChargeSummaryItem objects
     * @access private
     * @param string $startDate
     * @param string $endDate
     * @return array ChargeSummaryItem objects array
     */
    private function buildChargeSummary($startDate, $endDate)
    {
        $query = new Query();
        $subQuery = new Query();
        
        $chargeSummary = []; // Charge summary array
        
        if ($this->policyid != null) {
        
            // Build a query to summarise all charges raised against this insurance policy between the start and end dates
            
            // Build the sub-query
            $subQuery->select([
                'familymemberid' => 'profees.familymemberid',
                'policyid' => 'inspolicies.policyid',
                'claimcodeid' => 'insclaimcodes.claimcodeid',
                'sumvalue' => 'SUM(profees.chargevalue)',
                'visits' => 'COUNT(*)',
            ])
            ->from('profees')
            ->innerJoin('practitioners', 'practitioners.practitionerid = profees.practitionerid')
            ->innerJoin('familymembers', 'familymembers.memberid = profees.familymemberid')
            ->innerJoin('practtypes', 'practtypes.practtypeid = practitioners.practtypeid')
            ->innerJoin('insclaimcodes', 'insclaimcodes.claimcodeid = practtypes.claimcodeid')
            ->innerJoin('families', 'families.familyid = familymembers.familyid')
            ->innerJoin('inspolicies', 'inspolicies.policyid = families.policyid')
            ->where([
                'profees.claimable' => true,
            ])
            ->andWhere(['>=','profees.chargedate',$startDate])
            ->andWhere(['<=','profees.chargedate',$endDate])
            ->andWhere(['inspolicies.policyid' => $this->policyid])
            ->groupBy('profees.familymemberid, inspolicies.policyid, insclaimcodes.claimcodeid')
            ->orderBy('profees.familymemberid, inspolicies.policyid, insclaimcodes.claimcodeid')
            ;
            
            // Build the main query
            $query->select([
                'familymembers.memberid',
                'familymembers.name',
                'insclaimcodes.claimcodeid',
                'insclaimcodes.description',
                '"ChargeSummary".visits',
                '"ChargeSummary".sumvalue',
                'insccrules.maxvisits',
                'insccrules.maxclaimvalue',
                'insccrules.percentcover',
                'insccrules.maxpaypppa',
                
            ])
            ->from(['ChargeSummary' => $subQuery])
            ->innerJoin('familymembers', 'familymembers.memberid = "ChargeSummary".familymemberid')
            ->innerJoin('insclaimcodes', 'insclaimcodes.claimcodeid = "ChargeSummary".claimcodeid')
            ->innerJoin('insccrules', 'insccrules.policyid = "ChargeSummary".policyid AND insccrules.claimcodeid = "ChargeSummary".claimcodeid'
                )
            ;
            
            // Execute the query and save the results in the ChargeSummaryItem objects array
            $results = $query->all();
            
            foreach($results as $summaryRow) {
    
                $chargeSummaryItem = new ChargeSummaryItem();
                
                $chargeSummaryItem->familyMemberId = $summaryRow['memberid'];
                $chargeSummaryItem->claimCodeId = $summaryRow['claimcodeid'];
                $chargeSummaryItem->visits = $summaryRow['visits'];
                $chargeSummaryItem->totalValue = $summaryRow['sumvalue'];
                $chargeSummaryItem->maxVisits = (integer) $summaryRow['maxvisits'];
                $chargeSummaryItem->maxClaimableValue = (float) $summaryRow['maxclaimvalue'];
                $chargeSummaryItem->percentCover = (float) $summaryRow['percentcover'];
                $chargeSummaryItem->maxPayablePerAnnum = (float) $summaryRow['maxpaypppa'];
                
                $chargeSummary[$chargeSummaryItem->familyMemberId][$chargeSummaryItem->claimCodeId] = $chargeSummaryItem;
                
            }
        
        }
        
        return $chargeSummary;
        
    }
	
	/**
	 * Link the fees in the temporary fees table to the insurance claim
	 * @access public
	 * @return boolean TRUE if the fees were linked successfully
	 */
	public function linkFeesToClaim() {
		
		$returnCode = true;
		
		// Get the claim start and end dates
		$dateFormat = 'Y-m-d';
		$startDate = DateTime::createFromFormat($dateFormat, $this->startdate);
		$endDate = DateTime::createFromFormat($dateFormat, $this->enddate);
		
		// First, unlink any previously-linked fees that are now outside of the claim period
		$query = $this->getProfees();
		$fees = $query->all();
		
		foreach($fees as $profee) {
			
			$chargeDate = DateTime::createFromFormat($dateFormat, $profee->chargedate);
			if ($chargeDate < $startDate || $chargeDate > $endDate) {
				// The fee charge date is outside the current claim period, so set the claim id on the fee record to null
				$profee->insclaimid = null;
				$profee->save();
			}
			
		}
		
		// Now loop through the temporary fees table and link the corresponding professional fees records to this claim
		$query = $this->getTempFees();
		$fees = $query->all();
		
		foreach($fees as $tempfee) {
			
			// Validate the temporary fee charge date
			$chargeDate = DateTime::createFromFormat($dateFormat, $tempfee->chargedate);
			if ($chargeDate < $startDate || $chargeDate > $endDate) {
				// The fee charge date is outside the current claim period - skip it
				continue;
			}
			
			// Read the professional fees row
			$profee = Profees::findOne($tempfee->chargeid);
			if ($profee == null) {
				continue;
			}
			
			// Set the claim id and save the fees row
			$profee->insclaimid = $this->claimid;
			$profee->inspayment = $tempfee->inspayment;
			$profee->save();
			
		}
		
		return $returnCode;
		
	}
	
	
	/**
	 * Update the insurance claim statuses of the professional fees linked to this claim
	 * @access public
	 * @return boolean TRUE if the claim statuses were updated successfully
	 */
	public function updateFeeInsClaimStatuses() {
		
		$returnCode = true;
		$dateFormat = 'Y-m-d';
		$submissionDate = DateTime::createFromFormat($dateFormat, ( $this->claimdate == null ? '0001-01-01' : $this->claimdate));
		$query = $this->getProfees();
		$fees = $query->all();
		
		foreach($fees as $profee) {
			
			$chargeDate = DateTime::createFromFormat($dateFormat, $profee->chargedate);
			
			switch($profee->insclaimstatus) {
				
				case 'Not claimed':
					if ($chargeDate <= $submissionDate) {
						// The claim submission date is on or after the charge date, so assume the fee has been claimed
						$profee->insclaimstatus = 'Claimed';
						$profee->save();
						
					}
					
					break;
				
				case 'Claimed':
					if ($this->settled) {
						// The claim has been settled - mark all claimed fees as paid unless they've been marked as rejected
						$profee->insclaimstatus = 'Paid';
						$profee->save();
						
					}
			
			}
			
		}
		
		return $returnCode;
		
	}
	
	/**
	 * Build and return a claim summary data provider
	 * @access public
	 * @return yii\data\ActiveDataProvider
	 */
	public function claimSummaryProvider() {
		
		$query = new Query();
		
		// Build the query
		$query->select([
			'claimcodeid' => 'insclaimcodes.claimcodeid',
			'claimcodedesc' => 'insclaimcodes.description',
			'chargevalue' => 'SUM(profees.chargevalue)',
			'payable' => 'SUM(profees.inspayment)',
			'visits' => 'COUNT(*)',
		])
			->from('profees')
			->innerJoin('practitioners', 'practitioners.practitionerid = profees.practitionerid')
			->innerJoin('insclaims', 'insclaims.claimid = profees.insclaimid')
			->innerJoin('practtypes', 'practtypes.practtypeid = practitioners.practtypeid')
			->innerJoin('insclaimcodes', 'insclaimcodes.claimcodeid = practtypes.claimcodeid')
			->where([
                'profees.insclaimid' => $this->claimid,
            ])
            ->groupBy('insclaimcodes.claimcodeid,insclaimcodes.description')
            ->orderBy('insclaimcodes.claimcodeid')
            
        ;
		
        $claimSummaryProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 6,
            ],
            'sort' => [
                'attributes' => [
                    'claimcodeid', 
                    'claimcodedesc', 
                    'visits',
                    'chargevalue',
                    'payable',
                ],
            ],
            
        ]);
		
		return $claimSummaryProvider;
		
	}
	
	
}
