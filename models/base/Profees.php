<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "profees".
 *
 * @property integer $chargeid
 * @property integer $familymemberid
 * @property string $chargedate
 * @property string $chargevalue
 * @property string $notes
 * @property resource $receiptimage
 * @property string $taxclaimstatus
 * @property string $inspayment
 * @property string $taxrelief
 * @property integer $practitionerid
 * @property integer $insclaimid
 * @property boolean $claimable
 * @property string $insclaimstatus
 *
 * @property Practitioners $practitioner
 * @property Familymembers $familymember
 * @property Insclaims $insclaim
 */
class Profees extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profees';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['familymemberid', 'practitionerid', 'insclaimid'], 'integer'],
            [['familymemberid', 'practitionerid','chargedate'], 'required'],
            [['chargedate'], 'safe'],
            [['chargevalue', 'inspayment', 'taxrelief'], 'number'],
            [['chargevalue', 'inspayment', 'taxrelief'], 'default','value' => 0.00],
            [['notes', 'receiptimage', 'taxclaimstatus', 'insclaimstatus'], 'string'],
            [['taxclaimstatus', 'insclaimstatus'], 'default','value' => '0'],
            [['claimable'], 'boolean'],
            [['claimable'], 'default', 'value' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'chargeid' => 'Charge id',
            'familymemberid' => 'Family member',
            'chargedate' => 'Charge date',
            'chargevalue' => 'Value',
            'notes' => 'Notes',
            'receiptimage' => 'Receipt image',
            'taxclaimstatus' => 'Tax claim status',
            'inspayment' => 'Insurance payment',
            'taxrelief' => 'Tax relief',
            'practitionerid' => 'Medical practitioner',
            'insclaimid' => 'Insurance claim id',
            'claimable' => 'Fee claimable from insurance',
            'insclaimstatus' => 'Insurance claim status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPractitioner()
    {
        return $this->hasOne(\app\models\Practitioners::className(), ['practitionerid' => 'practitionerid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamilymember()
    {
        return $this->hasOne(\app\models\Familymembers::className(), ['memberid' => 'familymemberid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInsclaim()
    {
        return $this->hasOne(\app\models\Insclaims::className(), ['claimid' => 'insclaimid']);
    }
}
