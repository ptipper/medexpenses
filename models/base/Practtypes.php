<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "practtypes".
 *
 * @property integer $practtypeid
 * @property string $description
 * @property integer $claimcodeid
 * @property integer $taxexptypeid
 *
 * @property Taxexptypes $taxexptype
 * @property Insclaimcodes $claimcode
 * @property Practitioners[] $practitioners
 */
class Practtypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'practtypes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['claimcodeid', 'taxexptypeid'], 'required'],
            [['claimcodeid', 'taxexptypeid'], 'integer'],
            [['description'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'practtypeid' => 'Practitioner type id',
            'description' => 'Description',
            'claimcodeid' => 'Insurance claim code id',
            'taxexptypeid' => 'Tax expense type id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxexptype()
    {
        return $this->hasOne(\app\models\Taxexptypes::className(), ['taxexptypeid' => 'taxexptypeid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClaimcode()
    {
        return $this->hasOne(\app\models\Insclaimcodes::className(), ['claimcodeid' => 'claimcodeid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPractitioners()
    {
        return $this->hasMany(\app\models\Practitioners::className(), ['practtypeid' => 'practtypeid']);
    }
}
