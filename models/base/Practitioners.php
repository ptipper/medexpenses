<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "practitioners".
 *
 * @property integer $practitionerid
 * @property string $title
 * @property string $firstname
 * @property string $surname
 * @property integer $practtypeid
 *
 * @property Practtypes $practtype
 * @property Profees[] $profees
 */
class Practitioners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'practitioners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string'],
            [['practtypeid'], 'integer'],
            [['firstname', 'surname'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'practitionerid' => 'Practitioner id',
            'title' => 'Title',
            'firstname' => 'First name',
            'surname' => 'Surname',
            'practtypeid' => 'Practitioner type',
            'fullName' => 'Full name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPracttype()
    {
        return $this->hasOne(\app\models\Practtypes::className(), ['practtypeid' => 'practtypeid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfees()
    {
        return $this->hasMany(\app\models\Profees::className(), ['practitionerid' => 'practitionerid']);
    }
    
    /**
     * Returns the practitioner's full name
     * @return string
     */
    public function getFullName()
    {
        return $this->firstname . " " . $this->surname;
    }

    /**
     * Returns the practitioner's full name and title
     * @return string
     */
    public function getTitleFullName()
    {
        return $this->title . " " . $this->firstname . " " . $this->surname;
    }

    /**
     * Returns the practitioner's surname, followed by title and firstname
     * @return string
     */
    public function getSurnameTitleFirstName()
    {
        return $this->surname . ", " . $this->title . " " . $this->firstname;
    }
}
