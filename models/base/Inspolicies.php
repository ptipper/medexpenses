<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "inspolicies".
 *
 * @property integer $policyid
 * @property integer $insurancecoid
 * @property string $policyno
 * @property string $description
 *
 * @property Insurancecos $insuranceco
 * @property Families[] $families
 */
class Inspolicies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inspolicies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['insurancecoid'], 'integer'],
            [['policyno'], 'string', 'max' => 20],
            [['description'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'policyid' => 'Insurance policy id',
            'insurancecoid' => 'Insurance company id',
            'policyno' => 'Policy number',
            'description' => 'Policy description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInsuranceco()
    {
        return $this->hasOne(\app\models\Insurancecos::className(), ['insurancecoid' => 'insurancecoid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamilies()
    {
        return $this->hasMany(\app\models\Families::className(), ['policyid' => 'policyid']);
    }
    
    /**
     * 
     * @return string
     */
    public function getPolicyNumberDescription()
    {
        return $this->policyno . " - " . $this->description;
    }
}
