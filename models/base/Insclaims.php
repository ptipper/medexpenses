<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "insclaims".
 *
 * @property integer $claimid
 * @property string $claimref
 * @property string $claimdate
 * @property string $startdate
 * @property string $enddate
 * @property boolean $settled
 * @property integer $policyid
 *
 * @property Inspolicies $policy
 * @property Profees[] $profees
 */
class Insclaims extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'insclaims';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['claimdate', 'startdate', 'enddate'], 'safe'],
            [['settled'], 'boolean'],
            [['policyid'], 'required'],
            [['policyid'], 'integer'],
            [['claimref'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'claimid' => 'Insurance claim id',
            'claimref' => 'Claim reference',
            'claimdate' => 'Date claim submitted',
            'startdate' => 'Start date of claim period',
            'enddate' => 'End date of claim period',
            'settled' => 'Claim settled',
            'policyid' => 'Insurance policy id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicy()
    {
        return $this->hasOne(\app\models\Inspolicies::className(), ['policyid' => 'policyid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    private function getFees($feesClass)
    {
        $query = $this->hasMany($feesClass, ['insclaimid' => 'claimid']); 
        $query->joinWith('familymember', true, 'INNER JOIN');
        $query->joinWith('practitioner', true, 'INNER JOIN');
        
        return $query; 
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfees()
    {
        return $this->getFees(\app\models\Profees::className()); 
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTempfees()
    {
        return $this->getFees(\app\models\Tempfees::className()); 
    }
}
