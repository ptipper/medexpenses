<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "familymembers".
 *
 * @property integer $memberid
 * @property integer $familyid
 * @property string $name
 *
 * @property Families $family
 * @property Profees[] $profees
 * @property Prescriptions[] $prescriptions
 */
class Familymembers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'familymembers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['familyid'], 'integer'],
            [['name'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'memberid' => 'Family member id',
            'familyid' => 'Family id',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamily()
    {
        return $this->hasOne(\app\models\Families::className(), ['familyid' => 'familyid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfees()
    {
        return $this->hasMany(\app\models\Profees::className(), ['familymemberid' => 'memberid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrescriptions()
    {
        return $this->hasMany(\app\models\Prescriptions::className(), ['familymemberid' => 'memberid']);
    }
}
