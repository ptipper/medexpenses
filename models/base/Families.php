<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "families".
 *
 * @property integer $familyid
 * @property string $familyname
 * @property integer $policyid
 *
 * @property Familymembers[] $familymembers
 * @property Inspolicies $policy
 * @property Insclaims[] $insclaims
 */
class Families extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'families';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['policyid'], 'integer'],
            [['familyname'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'familyid' => 'Family id',
            'familyname' => 'Family name',
            'policyid' => 'Insurance policy id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamilymembers()
    {
        return $this->hasMany(\app\models\Familymembers::className(), ['familyid' => 'familyid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicy()
    {
        return $this->hasOne(\app\models\Inspolicies::className(), ['policyid' => 'policyid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInsclaims()
    {
        return $this->hasMany(\app\models\Insclaims::className(), ['familyid' => 'familyid']);
    }
}
