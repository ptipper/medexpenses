<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "prescriptions".
 *
 * @property integer $chargeid
 * @property integer $familymemberid
 * @property string $chargedate
 * @property string $chargevalue
 * @property string $notes
 * @property resource $receiptimage
 * @property string $taxclaimstatus
 * @property string $inspayment
 * @property string $taxrelief
 * @property integer $pharmacyid
 * @property string $formno
 *
 * @property Familymembers $familymember
 * @property Pharmacies $pharmacy
 */
class Prescriptions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prescriptions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['familymemberid', 'pharmacyid'], 'integer'],
            [['chargedate'], 'safe'],
            [['chargevalue', 'inspayment', 'taxrelief'], 'number'],
            [['notes', 'receiptimage', 'taxclaimstatus'], 'string'],
            [['formno'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'chargeid' => 'Charge id',
            'familymemberid' => 'Family member',
            'chargedate' => 'Charge date',
            'chargevalue' => 'Value',
            'notes' => 'Notes',
            'receiptimage' => 'Receipt image',
            'taxclaimstatus' => 'Tax claim status',
            'inspayment' => 'Insurance payment',
            'taxrelief' => 'Tax relief',
            'pharmacyid' => 'Pharmacy',
            'formno' => 'Prescription form number',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamilymember()
    {
        return $this->hasOne(\app\models\Familymembers::className(), ['memberid' => 'familymemberid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPharmacy()
    {
        return $this->hasOne(\app\models\Pharmacies::className(), ['pharmacyid' => 'pharmacyid']);
    }
}
