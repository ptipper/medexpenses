<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "insccrules".
 *
 * @property integer $claimcodeid
 * @property integer $policyid
 * @property integer $maxvisits
 * @property string $maxclaimvalue
 * @property string $percentcover
 * @property string $maxpaypppa
 *
 * @property Insclaimcodes $claimcode
 * @property Inspolicies $policy
 */
class Insccrules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'insccrules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['claimcodeid', 'policyid'], 'required'],
            [['claimcodeid', 'policyid', 'maxvisits'], 'integer'],
            [['maxclaimvalue', 'percentcover', 'maxpaypppa'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'claimcodeid' => 'Claim code id',
            'policyid' => 'Insurance policy id',
            'maxvisits' => 'Maximum number of visits per claim',
            'maxclaimvalue' => 'Maximum payable per person per visit',
            'percentcover' => 'Percentage cover',
            'maxpaypppa' => 'Maximum amount payable per person per annum',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClaimcode()
    {
        return $this->hasOne(\app\models\Insclaimcodes::className(), ['claimcodeid' => 'claimcodeid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPolicy()
    {
        return $this->hasOne(\app\models\Inspolicies::className(), ['policyid' => 'policyid']);
    }
}
