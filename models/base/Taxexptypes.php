<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "taxexptypes".
 *
 * @property integer $taxexptypeid
 * @property string $description
 * @property string $section
 * @property string $category
 * @property boolean $taxrelief
 *
 * @property Practtypes[] $practtypes
 */
class Taxexptypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taxexptypes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string', 'max' => 120],
            [['section', 'category'], 'string', 'max' => 10],
            [['taxrelief'], 'boolean'],
            [['taxrelief'], 'default', 'value' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'taxexptypeid' => 'Tax expense type id',
            'description' => 'Description',
            'section' => 'Section code',
            'category' => 'Category code',
            'taxrelief' => 'Eligible for tax relief',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPracttypes()
    {
        return $this->hasMany(\app\models\Practtypes::className(), ['taxexptypeid' => 'taxexptypeid']);
    }
}
