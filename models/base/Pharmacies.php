<?php

namespace app\models\base;

use Yii;

/**
 * This is the base-model class for table "pharmacies".
 *
 * @property integer $pharmacyid
 * @property string $name
 *
 * @property Prescriptions[] $prescriptions
 */
class Pharmacies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pharmacies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pharmacyid' => 'Pharmacy id',
            'name' => 'Pharmacy name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrescriptions()
    {
        return $this->hasMany(\app\models\Prescriptions::className(), ['pharmacyid' => 'pharmacyid']);
    }
}
