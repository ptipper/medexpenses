<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Insccrules;

/**
 * InsccrulesSearch represents the model behind the search form about `app\models\Insccrules`.
 */
class InsccrulesSearch extends Insccrules
{
    /**
     * @inheritdoc
     */
    
    public $policy;    // Insurance policies relation
    public $claimcode;  // Insurance claim codes relation
    
    public function rules()
    {
        return [
            [['claimcodeid', 'policyid', 'maxvisits'], 'integer'],
            [['maxclaimvalue', 'percentcover'], 'number'],
            [['policy','claimcode'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Insccrules::find();
        $query->joinWith('policy', true, 'INNER JOIN');
        $query->joinWith('claimcode', true, 'INNER JOIN');
        

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // Allow sorting by related columns
		$dataProvider->sort->attributes['policy'] = [
		    // The tables are the ones our relation are configured to
		    'asc' => ['inspolicies.description' => SORT_ASC],
		    'desc' => ['inspolicies.description' => SORT_DESC],
		];

		$dataProvider->sort->attributes['claimcode'] = [
		    // The tables are the ones our relation are configured to
		    'asc' => ['insclaimcodes.description' => SORT_ASC],
		    'desc' => ['insclaimcodes.description' => SORT_DESC],
		];

		$this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'claimcodeid' => $this->claimcodeid,
            'policyid' => $this->policyid,
            'maxvisits' => $this->maxvisits,
            'maxclaimvalue' => $this->maxclaimvalue,
            'percentcover' => $this->percentcover,
        ]);
        
        $query->andFilterWhere(['like', 'inspolicies.description', $this->policy])
              ->andFilterWhere(['like', 'insclaimcodes.description', $this->claimcode]);
        

        return $dataProvider;
    }
}
