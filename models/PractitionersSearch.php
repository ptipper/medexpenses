<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Practitioners;

/**
 * PractitionersSearch represents the model behind the search form about Practitioners.
 */
class PractitionersSearch extends Model
{
	public $practitionerid;
	public $title;
	public $firstname;
	public $surname;
	public $practtypeid;
	public $practtype;

	public function rules()
	{
		return [
			[['practitionerid', 'practtypeid'], 'integer'],
			[['title', 'firstname', 'surname', 'practtype'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'practitionerid' => 'Practitioner id',
			'title' => 'Title',
			'firstname' => 'First name',
			'surname' => 'Surname',
			'practtypeid' => 'Practitioner type',
		];
	}

	public function search($params)
	{
		$query = Practitioners::find();
		$query->joinWith('practtype', true, 'INNER JOIN');
		
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		
		// Allow sorting by insurance policy description
		$dataProvider->sort->attributes['practtype'] = [
		    // The tables are the ones our relation are configured to
		    'asc' => ['practtypes.description' => SORT_ASC],
		    'desc' => ['practtypes.description' => SORT_DESC],
		];

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'practitionerid' => $this->practitionerid,
            'practtypeid' => $this->practtypeid,
        ]);

		$query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'surname', $this->surname])
		    ->andFilterWhere(['like', 'practtypes.description', $this->practtype]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
