<?php

namespace app\models;

/** 
 * Medical charge summary item
 * @author tipperp
 * 
 */
class ChargeSummaryItem
{

    /**
     * Claim code id
     * @var integer
     */
    public $claimCodeId;
    
    /**
     * Family member id
     * @var integer
     */
    public $familyMemberId;
    
    /**
     * Total visits
     * @var integer
     */
    public $visits;
    
    /**
     * Total claim value
     * @var float
     */
    public $totalValue;
    
    /**
     * Maximum visits per insured per annum
     * @var integer
     */
    public $maxVisits;
    
    /**
     * Maximum claimable value per insured per annum
     * @var float
     */
    public $maxClaimableValue;
    
    /**
     * Percentage of claim value covered by policy
     * @var float
     */
    public $percentCover;
    
    /**
     * Maximum claimable value per insured per annum
     * @var float
     */
    public $maxPayablePerAnnum;
    
    /**
     * Constructor function for ChargeSummaryItem class
     */
    function __construct()
    {
        $this->claimCodeId = 0;
        $this->familyMemberId = 0;
        $this->visits = 0;
        $this->totalValue = 0.0;
        $this->maxVisits = 0;
        $this->maxClaimableValue = 0.0;
        $this->percentCover = 0.0;
        $this->maxPayablePerAnnum = 0.0;
        
    }
}

?>