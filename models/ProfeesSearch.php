<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Profees;

/**
 * ProfeesSearch represents the model behind the search form about Profees.
 */
class ProfeesSearch extends Model
{
	public $chargeid;
	public $familymemberid;
	public $familymember;      // Family model object
	public $chargedate;
	public $chargevalue;
	public $notes;
	public $receiptimage;
	public $taxclaimstatus;
	public $inspayment;
	public $taxrelief;
	public $practitionerid;
	public $practitioner;      // Practitioner model object
	public $insclaimid;
	public $insclaim;          // Insurance claim model object
	public $claimable;
	public $insclaimstatus;

	public function rules()
	{
		return [
			[['chargeid', 'familymemberid', 'practitionerid', 'insclaimid'], 'integer'],
			[['chargedate', 'notes', 'receiptimage', 'taxclaimstatus', 'insclaimstatus', 'familymember', 'practitioner', 'insclaim'], 'safe'],
			[['chargevalue', 'inspayment', 'taxrelief'], 'number'],
			[['claimable'], 'boolean'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'chargeid' => 'Charge id',
			'familymemberid' => 'Family member',
			'chargedate' => 'Charge date',
			'chargevalue' => 'Charge value',
			'notes' => 'Notes',
			'receiptimage' => 'Receipt image',
			'taxclaimstatus' => 'Tax claim status',
			'inspayment' => 'Insurance payment',
			'taxrelief' => 'Tax relief',
			'practitionerid' => 'Medical practitioner',
			'insclaimid' => 'Insurance claim id',
			'claimable' => 'Fee claimable from insurance',
			'insclaimstatus' => 'Insurance status',
		];
	}

	public function search($params)
	{
		$query = Profees::find();
		$query->joinWith('familymember', true, 'INNER JOIN');
		$query->joinWith('practitioner', true, 'INNER JOIN');
		$query->joinWith('insclaim', true, 'LEFT JOIN');
		$query->orderBy(['chargeid' => SORT_DESC]);
		
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		// Allow sorting by related columns
		$dataProvider->sort->attributes['familymember'] = [
		    // The tables are the ones our relation are configured to
		    'asc' => ['familymembers.name' => SORT_ASC],
		    'desc' => ['familymembers.name' => SORT_DESC],
		];

		$dataProvider->sort->attributes['practitioner'] = [
		    // The tables are the ones our relation are configured to
		    'asc' => ['practitioners.surname' => SORT_ASC],
		    'desc' => ['practitioners.surname' => SORT_DESC],
		];

		$dataProvider->sort->attributes['insclaim'] = [
		    // The tables are the ones our relation are configured to
		    'asc' => ['insclaims.claimref' => SORT_ASC],
		    'desc' => ['insclaims.claimref' => SORT_DESC],
		];

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		$query->andFilterWhere([
            'chargeid' => $this->chargeid,
            'familymemberid' => $this->familymemberid,
            'chargedate' => $this->chargedate,
            'chargevalue' => $this->chargevalue,
            'inspayment' => $this->inspayment,
            'taxrelief' => $this->taxrelief,
            'practitionerid' => $this->practitionerid,
            'insclaimid' => $this->insclaimid,
            'claimable' => $this->claimable,
        ]);

		$query->andFilterWhere(['like', 'notes', $this->notes])
            ->andFilterWhere(['like', 'receiptimage', $this->receiptimage])
            ->andFilterWhere(['like', 'taxclaimstatus', $this->taxclaimstatus])
            ->andFilterWhere(['like', 'insclaimstatus', $this->insclaimstatus])
		    ->andFilterWhere(['like', 'familymembers.name', $this->familymember])
		    ->andFilterWhere(['like', 'practitioners.surname', $this->practitioner])
		    ->andFilterWhere(['like', 'insclaims.claimref', $this->insclaim]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
