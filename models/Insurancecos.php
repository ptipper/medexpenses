<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "insurancecos".
 *
 * @property integer $insurancecoid
 * @property string $name
 *
 * @property Inspolicies[] $inspolicies
 */
class Insurancecos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'insurancecos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'insurancecoid' => 'Insurance company id',
            'name' => 'Company name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspolicies()
    {
        return $this->hasMany(Inspolicies::className(), ['insurancecoid' => 'insurancecoid']);
    }
}
