<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Familymembers;

/**
 * FamilymembersSearch represents the model behind the search form about Familymembers.
 */
class FamilymembersSearch extends Model
{
	public $memberid;
	public $familyid;
	public $name;
	public $family;

	public function rules()
	{
		return [
			[['memberid', 'familyid'], 'integer'],
			[['name', 'family'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'memberid' => 'Family member id',
			'familyid' => 'Family',
			'name' => 'Name',
		];
	}

	public function search($params)
	{
		$query = Familymembers::find();
		$query->joinWith('family', true, 'INNER JOIN');
		
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		// Allow sorting by insurance policy description
		$dataProvider->sort->attributes['family'] = [
		    // The tables are the ones our relation are configured to
		    'asc' => ['families.familyname' => SORT_ASC],
		    'desc' => ['families.familyname' => SORT_DESC],
		];

		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}
		
		$query->andFilterWhere([
            'memberid' => $this->memberid,
            'familyid' => $this->familyid,
        ]);

		$query->andFilterWhere(['like', 'name', $this->name])
		      ->andFilterWhere(['like', 'families.familyname', $this->family]);

		return $dataProvider;
	}

	protected function addCondition($query, $attribute, $partialMatch = false)
	{
		$value = $this->$attribute;
		if (trim($value) === '') {
			return;
		}
		if ($partialMatch) {
			$value = '%' . strtr($value, ['%'=>'\%', '_'=>'\_', '\\'=>'\\\\']) . '%';
			$query->andWhere(['like', $attribute, $value]);
		} else {
			$query->andWhere([$attribute => $value]);
		}
	}
}
