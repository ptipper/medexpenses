<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tempfees".
 *
 * @property integer $chargeid
 * @property integer $familymemberid
 * @property string $chargedate
 * @property string $chargevalue
 * @property string $notes
 * @property resource $receiptimage
 * @property string $taxclaimstatus
 * @property string $inspayment
 * @property string $taxrelief
 * @property integer $practitionerid
 * @property integer $insclaimid
 * @property boolean $claimable
 * @property string $insclaimstatus
 *
 * @property Familymembers $familymember
 * @property Insclaims $insclaim
 * @property Practitioners $practitioner
 */
class Tempfees extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tempfees';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['familymemberid', 'practitionerid', 'insclaimid'], 'integer'],
            [['chargedate'], 'safe'],
            [['chargevalue', 'inspayment', 'taxrelief'], 'number'],
            [['notes', 'receiptimage', 'taxclaimstatus', 'insclaimstatus'], 'string'],
            [['claimable'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'chargeid' => 'Chargeid',
            'familymemberid' => 'Familymemberid',
            'chargedate' => 'Chargedate',
            'chargevalue' => 'Chargevalue',
            'notes' => 'Notes',
            'receiptimage' => 'Receiptimage',
            'taxclaimstatus' => 'Taxclaimstatus',
            'inspayment' => 'Inspayment',
            'taxrelief' => 'Taxrelief',
            'practitionerid' => 'Medical practitioner',
            'insclaimid' => 'Insurance claim id',
            'claimable' => 'Fee claimable from insurance',
            'insclaimstatus' => 'Insurance claim status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamilymember()
    {
        return $this->hasOne(Familymembers::className(), ['memberid' => 'familymemberid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInsclaim()
    {
        return $this->hasOne(Insclaims::className(), ['claimid' => 'insclaimid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPractitioner()
    {
        return $this->hasOne(Practitioners::className(), ['practitionerid' => 'practitionerid']);
    }
}
