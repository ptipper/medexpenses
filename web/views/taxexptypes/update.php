<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Taxexptypes $model
 */

$this->title = 'Taxexptypes Update ' . $model->taxexptypeid . '';
$this->params['breadcrumbs'][] = ['label' => 'Taxexptypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->taxexptypeid, 'url' => ['view', 'taxexptypeid' => $model->taxexptypeid]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="taxexptypes-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'taxexptypeid' => $model->taxexptypeid], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
