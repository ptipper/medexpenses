<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Profees $model
 */

$this->title = 'Profees Update ' . $model->chargeid . '';
$this->params['breadcrumbs'][] = ['label' => 'Profees', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->chargeid, 'url' => ['view', 'chargeid' => $model->chargeid]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="profees-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'chargeid' => $model->chargeid], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
