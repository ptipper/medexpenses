<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\ProfeesSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="profees-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'chargeid') ?>

		<?= $form->field($model, 'familymemberid') ?>

		<?= $form->field($model, 'chargedate') ?>

		<?= $form->field($model, 'chargevalue') ?>

		<?= $form->field($model, 'notes') ?>

		<?php // echo $form->field($model, 'receiptimage') ?>

		<?php // echo $form->field($model, 'taxclaimstatus') ?>

		<?php // echo $form->field($model, 'inspayment') ?>

		<?php // echo $form->field($model, 'taxrelief') ?>

		<?php // echo $form->field($model, 'practitionerid') ?>

		<?php // echo $form->field($model, 'insclaimid') ?>

		<?php // echo $form->field($model, 'claimable')->checkbox() ?>

		<?php // echo $form->field($model, 'insclaimstatus') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
