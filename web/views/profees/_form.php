<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
* @var yii\web\View $this
* @var app\models\Profees $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="profees-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>
            
			<?= $form->field($model, 'familymemberid')->textInput() ?>
			<?= $form->field($model, 'practitionerid')->textInput() ?>
			<?= $form->field($model, 'insclaimid')->textInput() ?>
			<?= $form->field($model, 'chargedate')->textInput() ?>
			<?= $form->field($model, 'chargevalue')->textInput() ?>
			<?= $form->field($model, 'inspayment')->textInput() ?>
			<?= $form->field($model, 'taxrelief')->textInput() ?>
			<?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>
			<?= $form->field($model, 'receiptimage')->textInput() ?>
			<?= $form->field($model, 'taxclaimstatus')->dropDownList([ 'Not claimed' => 'Not claimed', 'Claimed' => 'Claimed', 'Paid' => 'Paid', 'Rejected' => 'Rejected', ], ['prompt' => '']) ?>
			<?= $form->field($model, 'insclaimstatus')->dropDownList([ 'Not claimed' => 'Not claimed', 'Claimed' => 'Claimed', 'Paid' => 'Paid', 'Rejected' => 'Rejected', ], ['prompt' => '']) ?>
			<?= $form->field($model, 'claimable')->checkbox() ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    \yii\bootstrap\Tabs::widget(
                 [
                   'encodeLabels' => false,
                     'items' => [ [
    'label'   => 'Profees',
    'content' => $this->blocks['main'],
    'active'  => true,
], ]
                 ]
    );
    ?>
        <hr/>

        <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> '.($model->isNewRecord ? 'Create' : 'Save'), ['class' => $model->isNewRecord ?
        'btn btn-primary' : 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
