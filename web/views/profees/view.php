<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var app\models\Profees $model
*/

$this->title = 'Profees View ' . $model->chargeid . '';
$this->params['breadcrumbs'][] = ['label' => 'Profees', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->chargeid, 'url' => ['view', 'chargeid' => $model->chargeid]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="profees-view">

    <p class='pull-left'>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Edit', ['update', 'chargeid' => $model->chargeid],
        ['class' => 'btn btn-info']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Profees', ['create'], ['class' => 'btn
        btn-success']) ?>
    </p>

        <p class='pull-right'>
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['index'], ['class'=>'btn btn-default']) ?>
    </p><div class='clearfix'></div> 

    
    <h3>
        <?= $model->chargeid ?>    </h3>


    <?php $this->beginBlock('app\models\Profees'); ?>

    <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    			'chargeid',
			'familymemberid',
			'chargedate',
			'chargevalue',
			'notes:ntext',
			'receiptimage',
			'taxclaimstatus',
			'inspayment',
			'taxrelief',
			'practitionerid',
			'insclaimid',
			'claimable:boolean',
			'insclaimstatus',
    ],
    ]); ?>

    <hr/>

    <?php echo Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['delete', 'chargeid' => $model->chargeid],
    [
    'class' => 'btn btn-danger',
    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
    'data-method' => 'post',
    ]); ?>

    <?php $this->endBlock(); ?>


    
    <?=
    \yii\bootstrap\Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<span class="glyphicon glyphicon-asterisk"></span> Profees',
    'content' => $this->blocks['app\models\Profees'],
    'active'  => true,
], ]
                 ]
    );
    ?></div>
