<?php

namespace app\controllers;

use Yii;
use app\models\Insccrules;
use app\models\InsccrulesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * InsccrulesController implements the CRUD actions for Insccrules model.
 */
class InsccrulesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Insccrules models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InsccrulesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Insccrules model.
     * @param integer $claimcodeid
     * @param integer $policyid
     * @return mixed
     */
    public function actionView($claimcodeid, $policyid)
    {
        return $this->render('view', [
            'model' => $this->findModel($claimcodeid, $policyid),
        ]);
    }

    /**
     * Creates a new Insccrules model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Insccrules();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'claimcodeid' => $model->claimcodeid, 'policyid' => $model->policyid]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Insccrules model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $claimcodeid
     * @param integer $policyid
     * @return mixed
     */
    public function actionUpdate($claimcodeid, $policyid)
    {
        $model = $this->findModel($claimcodeid, $policyid);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'claimcodeid' => $model->claimcodeid, 'policyid' => $model->policyid]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Insccrules model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $claimcodeid
     * @param integer $policyid
     * @return mixed
     */
    public function actionDelete($claimcodeid, $policyid)
    {
        $this->findModel($claimcodeid, $policyid)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Insccrules model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $claimcodeid
     * @param integer $policyid
     * @return Insccrules the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($claimcodeid, $policyid)
    {
        if (($model = Insccrules::findOne(['claimcodeid' => $claimcodeid, 'policyid' => $policyid])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
