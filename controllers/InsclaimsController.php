<?php

namespace app\controllers;

use Yii;
use app\models\Insclaims;
use app\models\InsclaimsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\models\app\models;

/**
 * InsclaimsController implements the CRUD actions for Insclaims model.
 */
class InsclaimsController extends Controller
{
    public $jsFile;

    public function init()
    {
        parent::init();
        
        // Load the ajax Javascript source file
        $this->jsFile = '@app/views/' . $this->id . '/ajax.js';
        
        // Publish and register the required JS file
        Yii::$app->assetManager->publish($this->jsFile);
        $this->getView()->registerJsFile(Yii::$app->assetManager->getPublishedUrl($this->jsFile), [
            'yii\web\YiiAsset'
        ]);
        
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Insclaims models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InsclaimsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Insclaims model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Insclaims model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Insclaims();
        $model->loadCharges(!(Yii::$app->request->isAjax));

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->claimid]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'chargesImported' => 0,
            ]);
        }
    }

    /**
     * Updates an existing Insclaims model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $ajaxRequest = Yii::$app->request->isAjax;
        $submit = Yii::$app->request->post("claimid") != null ? true : false;
        
        $model = $this->findModel($id, !($ajaxRequest || $submit));

        if ($model->load(Yii::$app->request->post()) && $model->save() && $model->linkFeesToClaim() && $model->updateFeeInsClaimStatuses()) {
            return $this->redirect(['view', 'id' => $model->claimid]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'chargesImported' => 0,
            ]);
        }
    }

    /**
     * Deletes an existing Insclaims model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Insclaims model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param boolean $initTempFees If true, then initialise the temporary fees table and populate it with charges associated with this claim
     * @return Insclaims the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $initTempFees = false)
    {
        if (($model = Insclaims::findOne($id)) !== null) {
            
            $model->loadCharges($initTempFees);
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionImport()
    {
        $responseMessage = '';
        
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            
            // Get the insurance claim id
            $id = (int) Yii::$app->request->post('claimid', 0);
            if ($id != 0) {
                // Existing claim - load the current row
                $model = $this->findModel($id);
            }
            else {
                $model = new Insclaims();
            }
            
            // Merge the posted form data with the model object
            if ($model->load(Yii::$app->request->post())) {
                
                // Import medical charges
                try {
                    $chargesImported = $model->importCharges($model->startdate, $model->enddate);
                    $responseMessage = "$chargesImported fees imported.";
                    $success = true;
                }
                catch (\Exception $e) {
                    $responseMessage = $e->getMessage();
                    $success = false;
                }
                
            }
            
            $res = array(
                'body' => $responseMessage,
                'success' => $success,
            );
            
            return $res;
        }
        
    }
}
