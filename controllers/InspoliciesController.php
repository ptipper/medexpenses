<?php

namespace app\controllers;

use app\models\Inspolicies;
use app\models\InspoliciesSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * InspoliciesController implements the CRUD actions for Inspolicies model.
 */
class InspoliciesController extends Controller
{
	/**
	 * Lists all Inspolicies models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new InspoliciesSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single Inspolicies model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($policyid)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($policyid),
		]);
	}

	/**
	 * Creates a new Inspolicies model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Inspolicies;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing Inspolicies model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($policyid)
	{
		$model = $this->findModel($policyid);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Inspolicies model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($policyid)
	{
		$this->findModel($policyid)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the Inspolicies model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Inspolicies the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($policyid)
	{
		if (($model = Inspolicies::findOne($policyid)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
