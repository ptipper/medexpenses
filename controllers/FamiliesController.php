<?php

namespace app\controllers;

use app\models\Families;
use app\models\FamiliesSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * FamiliesController implements the CRUD actions for Families model.
 */
class FamiliesController extends Controller
{
	/**
	 * Lists all Families models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new FamiliesSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single Families model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($familyid)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($familyid),
		]);
	}

	/**
	 * Creates a new Families model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Families;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing Families model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($familyid)
	{
		$model = $this->findModel($familyid);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Families model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($familyid)
	{
		$this->findModel($familyid)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the Families model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Families the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($familyid)
	{
		if (($model = Families::findOne($familyid)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
