<?php

namespace app\controllers;

use app\models\Profees;
use app\models\ProfeesSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * ProfeesController implements the CRUD actions for Profees model.
 */
class ProfeesController extends Controller
{
	/**
	 * Lists all Profees models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ProfeesSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single Profees model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($chargeid)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($chargeid),
		]);
	}

	/**
	 * Creates a new Profees model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Profees;
		
		// Set defaults
		$model->claimable = true;
		$model->insclaimstatus = 'Not claimed';
		$model->taxclaimstatus = 'Not claimed';
		
		try {
            if ($model->load($_POST) && $model->save()) {
                // Re-load the fee creation form, to facilitate batch entry of medical fees
                return $this->redirect(Url::current());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing Profees model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($chargeid)
	{
		$model = $this->findModel($chargeid);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Profees model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($chargeid)
	{
		$this->findModel($chargeid)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the Profees model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Profees the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($chargeid)
	{
		if (($model = Profees::findOne($chargeid)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
