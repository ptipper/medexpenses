<?php

namespace app\controllers;

use app\models\TaxClaim;
use yii\web\Controller;
use yii\helpers\Url;

/**
 * TaxClaimController is a controller for generating and displaying tax claims
 */
class TaxClaimController extends Controller
{
    /**
	 * Displays a single TaxClaim model.
	 * @param integer $taxYear
	 * @return mixed
	 */
	public function actionView($taxYear = 0, $familyId = 0)
	{
        Url::remember();
		
		$model = new TaxClaim($taxYear, $familyId);
		
        if ($model->load($_POST) && $model->calculateTaxClaim()) {
            return $this->render('view', [
				'model' => $model,
			]);
		} else {
			return $this->render('selectyear', [
				'model' => $model,
			]);
		}
	}

}
?>