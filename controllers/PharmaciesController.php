<?php

namespace app\controllers;

use app\models\Pharmacies;
use app\models\PharmaciesSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * PharmaciesController implements the CRUD actions for Pharmacies model.
 */
class PharmaciesController extends Controller
{
	/**
	 * Lists all Pharmacies models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new PharmaciesSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single Pharmacies model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($pharmacyid)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($pharmacyid),
		]);
	}

	/**
	 * Creates a new Pharmacies model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Pharmacies;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing Pharmacies model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($pharmacyid)
	{
		$model = $this->findModel($pharmacyid);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Pharmacies model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($pharmacyid)
	{
		$this->findModel($pharmacyid)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the Pharmacies model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Pharmacies the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($pharmacyid)
	{
		if (($model = Pharmacies::findOne($pharmacyid)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
