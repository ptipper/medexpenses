<?php

namespace app\controllers;

use app\models\Practitioners;
use app\models\PractitionersSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * PractitionersController implements the CRUD actions for Practitioners model.
 */
class PractitionersController extends Controller
{
	/**
	 * Lists all Practitioners models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new PractitionersSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single Practitioners model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($practitionerid)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($practitionerid),
		]);
	}

	/**
	 * Creates a new Practitioners model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Practitioners;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing Practitioners model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($practitionerid)
	{
		$model = $this->findModel($practitionerid);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Practitioners model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($practitionerid)
	{
		$this->findModel($practitionerid)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the Practitioners model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Practitioners the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($practitionerid)
	{
		if (($model = Practitioners::findOne($practitionerid)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
