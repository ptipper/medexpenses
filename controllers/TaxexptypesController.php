<?php

namespace app\controllers;

use app\models\Taxexptypes;
use app\models\TaxexptypesSearch;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * TaxexptypesController implements the CRUD actions for Taxexptypes model.
 */
class TaxexptypesController extends Controller
{
	/**
	 * Lists all Taxexptypes models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new TaxexptypesSearch;
		$dataProvider = $searchModel->search($_GET);

        Url::remember();
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	/**
	 * Displays a single Taxexptypes model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($taxexptypeid)
	{
        Url::remember();
        return $this->render('view', [
			'model' => $this->findModel($taxexptypeid),
		]);
	}

	/**
	 * Creates a new Taxexptypes model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Taxexptypes;

		try {
            if ($model->load($_POST) && $model->save()) {
                return $this->redirect(Url::previous());
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
        return $this->render('create', ['model' => $model,]);
	}

	/**
	 * Updates an existing Taxexptypes model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($taxexptypeid)
	{
		$model = $this->findModel($taxexptypeid);

		if ($model->load($_POST) && $model->save()) {
            return $this->redirect(Url::previous());
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Taxexptypes model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($taxexptypeid)
	{
		$this->findModel($taxexptypeid)->delete();
		return $this->redirect(Url::previous());
	}

	/**
	 * Finds the Taxexptypes model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Taxexptypes the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($taxexptypeid)
	{
		if (($model = Taxexptypes::findOne($taxexptypeid)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
