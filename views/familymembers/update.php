<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Familymembers $model
 */

$this->title = 'Familymembers Update ' . $model->name . '';
$this->params['breadcrumbs'][] = ['label' => 'Familymembers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->name, 'url' => ['view', 'memberid' => $model->memberid]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="familymembers-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'memberid' => $model->memberid], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
