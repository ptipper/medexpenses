<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Profees $model
 */

$this->title = 'Professional Fees Update ' . $model->chargeid . '';
$this->params['breadcrumbs'][] = ['label' => 'Professional fees', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->chargeid, 'url' => ['view', 'chargeid' => $model->chargeid]];
$this->params['breadcrumbs'][] = 'Edit';

// Publish and register the required JS file
$jsFile = '@app/views/profees/setcontrols.js';
Yii::$app->assetManager->publish($jsFile);
$this->registerJsFile(Yii::$app->assetManager->getPublishedUrl($jsFile),
    ['depends' => [\yii\web\JqueryAsset::className()],
        
    ]);
?>
<div class="profees-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'chargeid' => $model->chargeid], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
