<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Familymembers;
use app\models\Practitioners;
use kartik\money\MaskMoney;
use kartik\datecontrol\DateControl;

/**
* @var yii\web\View $this
* @var app\models\Profees $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="profees-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>
            <!-- Family member -->
			<?= $form->field($model, 'familymemberid', 
			         ['inputOptions' => [
			             'autofocus' => 'autofocus', 
			             'class' => 'form-control', 
			             'tabindex' => '1']
			         ]
			    )->dropDownList(
                    ArrayHelper::map(Familymembers::find()->all(), 'memberid','name'),
                    ['prompt' => 'Select family member']
			    ) 
			?>
			
			<!-- Medical practitioner -->
			<?= $form->field($model, 'practitionerid', 
			         ['inputOptions' => [
			             'tabindex' => '2']
			         ]
			)->dropDownList(
                    ArrayHelper::map(Practitioners::find()->orderBy('surname')->all(), 'practitionerid','surnameTitleFirstName'),
                    ['prompt' => 'Select practitioner']
			    ) 
			?>
			
            <!-- Charge date -->
            <?php $field = 'chargedate'; ?>
			<?= $form->field($model, $field)->begin(); ?>
                <?= Html::activeLabel($model, $field,['class' => 'control-label col-sm-3']); ?>
                <div class='col-xs-3' style='height: 44px;'>
                    <?= DateControl::widget([
                        'model' => $model,
                        'attribute' => $field,
    			        'type'=>DateControl::FORMAT_DATE,
    			        'ajaxConversion'=>false,
    			        'options' => [
    			            // 'id' => 'chargedate_input',
    			            'pluginOptions' => [
    			                'autoclose' => true,
    			            ],
        			    ],
    			    ]); 
                    ?>
                </div>
            <?= $form->field($model, $field)->end(); ?>
            
			<!-- Charge value -->
			<?php $field = 'chargevalue'; ?>
			<?= $form->field($model, $field)->begin(); ?>
                <?= Html::activeLabel($model, $field,['class' => 'control-label col-sm-3']); ?>
                <div class='col-xs-3' style='height: 44px;'>
                    <?= MaskMoney::widget([
                        'model' => $model,
                        'attribute' => $field,
                        'pluginOptions' => [
                            'thousands' => ',',
                            'decimal' => '.',
                            'precision' => 2, 
                            'allowZero' => true,
                            'allowNegative' => true,
                            ],
                    ]); 
                    ?>
                </div>
            <?= $form->field($model, $field)->end(); ?>
            
			<!-- Notes -->
			<?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>
			
			<!-- Taxation claim status -->
			<?= $form->field($model, 'taxclaimstatus')->dropDownList([ 'Not claimed' => 'Not claimed', 'Claimed' => 'Claimed', 'Paid' => 'Paid', 'Rejected' => 'Rejected', ], ['prompt' => '']) ?>
			
        </p>
        <?php $this->endBlock(); ?>
		
		<?php $this->beginBlock('insurance'); ?>

			<!-- Claimable from insurance -->
			<?= $form->field($model, 'claimable')->checkbox() ?>
			
			<!-- Amount claimable from insurance -->
			<?php $field = 'inspayment'; ?>
			<?= $form->field($model, $field)->begin(); ?>
                <?= Html::activeLabel($model, $field,['class' => 'control-label col-sm-3']); ?>
                <div class='col-xs-3' style='height: 44px;'>
                    <?= MaskMoney::widget([
                        'model' => $model,
                        'attribute' => $field,
                        'pluginOptions' => [
                            'thousands' => ',',
                            'decimal' => '.',
                            'precision' => 2, 
                            'allowZero' => true,
                            'allowNegative' => true,
                            ],
                    ]); 
                    ?>
                </div>
            <?= $form->field($model, $field)->end(); ?>
            
			<!-- Insurance claim status -->
			<?= $form->field($model, 'insclaimstatus')->dropDownList([ 'Not claimed' => 'Not claimed', 'Claimed' => 'Claimed', 'Paid' => 'Paid', 'Rejected' => 'Rejected', ], ['prompt' => '']) ?>

        <?php $this->endBlock(); ?>
        
        <?=
            \yii\bootstrap\Tabs::widget(
                 [
                   'encodeLabels' => false,
                     'items' => 
                     [ 
                             [
                                'label'   => 'Charge Details',
                                'content' => $this->blocks['main'],
                                'active'  => true,
                             ],
							 [
                                'label'   => 'Insurance',
                                'content' => $this->blocks['insurance'],
                             ],
                     ]
                 ]
    );
    ?>
        <hr/>

        <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> '.($model->isNewRecord ? 'Create' : 'Save'), 
            ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary',
             'id' => 'submitbutton',
        ]) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
