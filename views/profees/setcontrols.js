/**
 * setcontrols.js - Set form control attributes
 */

$(document).ready(function(){
	// Set the tabindex attributes of the Charge Date and subsequent input controls
	$("#profees-chargedate-disp").attr("tabindex", "3");
	$("#profees-chargevalue-disp").attr("tabindex", "4");
	$("#profees-notes").attr("tabindex", "5");
	$("#profees-claimable").attr("tabindex", "6");
	$("#profees-taxclaimstatus").attr("tabindex", "7");
	$("#profees-insclaimstatus").attr("tabindex", "8");
	$("#submitbutton").attr("tabindex", "9");
}); 