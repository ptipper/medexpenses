<?php

use yii\helpers\Html;

/**
* @var yii\web\View $this
* @var app\models\Profees $model
*/

$this->title = 'Create';
$this->params['breadcrumbs'][] = ['label' => 'Professional fees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

// Publish and register the required JS file
$jsFile = '@app/views/profees/setcontrols.js';
Yii::$app->assetManager->publish($jsFile);
$this->registerJsFile(Yii::$app->assetManager->getPublishedUrl($jsFile),
    ['depends' => [\yii\web\JqueryAsset::className()],
        
    ]);
?>
<div class="profees-create">

    <p class="pull-left">
        <?= Html::a('Cancel', \yii\helpers\Url::previous(), ['class' => 'btn btn-default']) ?>
    </p>
    <div class="clearfix"></div>

    <?php echo $this->render('_form', [
    'model' => $model,
    ]); ?>

</div>
