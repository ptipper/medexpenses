<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PrescriptionsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prescriptions-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'chargeid') ?>

    <?= $form->field($model, 'familymemberid') ?>

    <?= $form->field($model, 'chargedate') ?>

    <?= $form->field($model, 'chargevalue') ?>

    <?= $form->field($model, 'notes') ?>

    <?php // echo $form->field($model, 'receiptimage') ?>

    <?php // echo $form->field($model, 'taxclaimstatus') ?>

    <?php // echo $form->field($model, 'inspayment') ?>

    <?php // echo $form->field($model, 'taxrelief') ?>

    <?php // echo $form->field($model, 'pharmacyid') ?>

    <?php // echo $form->field($model, 'formno') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
