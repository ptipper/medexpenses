<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Prescriptions */

$this->title = 'Create Prescriptions';
$this->params['breadcrumbs'][] = ['label' => 'Prescriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

// Publish and register the required JS file
$jsFile = '@app/views/prescriptions/setcontrols.js';
Yii::$app->assetManager->publish($jsFile);
$this->registerJsFile(Yii::$app->assetManager->getPublishedUrl($jsFile),
    ['depends' => [\yii\web\JqueryAsset::className()],
        
    ]);
?>
<div class="prescriptions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
