<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PrescriptionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Prescriptions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prescriptions-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Enter prescriptions', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'export' => false,
        'condensed' => true,
        'columns' => [
            [
             'attribute' => 'chargeid',
             'value' => 'chargeid',
			 'label' => 'Id',
 			 'width' => '60px',
            ],
            [
             'attribute' => 'formno',
             'value' => 'formno',
			 'label' => 'Form no.',
 			 'width' => '100px',
            ],
            [
             'attribute' => 'familymember',
             'value' => 'familymember.name',
			 'label' => 'Person',
			 'width' => '80px',
            ],
            [
             'attribute' => 'pharmacy',
             'value' => 'pharmacy.name',
			 'label' => 'Pharmacy'
            ],
            [
             'attribute' => 'chargedate',
             'value' => 'chargedate',
			 'label' => 'Date',
             'format' => 'date',
 			 'width' => '100px',
           ],
            [
             'class' => '\kartik\grid\DataColumn',
             'attribute' => 'chargevalue',
             'hAlign' => 'right',
             'value' => 'chargevalue',
			 'label' => 'Value',
              'format' => ['decimal',2],
 			 'width' => '100px',
            ],
            'notes:ntext',
			[
             'attribute' => 'taxclaimstatus',
             'value' => 'taxclaimstatus',
			 'label' => 'Tax status',
 			 'width' => '100px',
            ],
            // 'receiptimage',
            // 'taxclaimstatus',
            // 'inspayment',
            // 'taxrelief',
            // 'pharmacyid',
            // 'formno',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
