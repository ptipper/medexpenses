/**
 * setcontrols.js - Set form control attributes
 */

$(document).ready(function(){
	// Set the tabindex attributes of the Charge Date and subsequent input controls
	$("#prescriptions-chargedate-disp").attr("tabindex", "4");
	$("#prescriptions-chargevalue-disp").attr("tabindex", "5");
	$("#prescriptions-notes").attr("tabindex", "6");
	$("#prescriptions-taxclaimstatus").attr("tabindex", "7");
	$("#submitbutton").attr("tabindex", "8");
}); 