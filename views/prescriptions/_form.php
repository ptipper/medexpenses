<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Familymembers;
use app\models\Pharmacies;
use kartik\money\MaskMoney;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\Prescriptions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prescriptions-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <?= $form->field($model, 'formno', 
			         ['inputOptions' => [
			             'autofocus' => 'autofocus', 
			             'class' => 'form-control', 
			             'tabindex' => '1']
			         ])->textInput(['maxlength' => 20]) ?>

    <!-- Family member -->
    <?= $form->field($model, 'familymemberid', 
             ['inputOptions' => [
                 'autofocus' => 'autofocus', 
                 'class' => 'form-control', 
                 'tabindex' => '2']
             ]
        )->dropDownList(
            ArrayHelper::map(Familymembers::find()->all(), 'memberid','name'),
            ['prompt' => 'Select family member']
        ) 
    ?>
			
	<!-- Pharmacy -->
    <?= $form->field($model, 'pharmacyid', 
             ['inputOptions' => [
                 'tabindex' => '3']
             ]
    )->dropDownList(
            ArrayHelper::map(Pharmacies::find()->orderBy('name')->all(), 'pharmacyid','name'),
            ['prompt' => 'Select pharmacy']
        ) 
    ?>
    
    <!-- Charge date -->
    <?php $field = 'chargedate'; ?>
    <?= $form->field($model, $field)->begin(); ?>
        <?= Html::activeLabel($model, $field,['class' => 'control-label col-sm-3']); ?>
        <div class='col-xs-3' style='height: 44px;'>
            <?= DateControl::widget([
                'model' => $model,
                'attribute' => $field,
                'type'=>DateControl::FORMAT_DATE,
                'ajaxConversion'=>false,
                'options' => [
                    // 'id' => 'chargedate_input',
                    'pluginOptions' => [
                        'autoclose' => true,
                    ],
                ],
            ]); 
            ?>
        </div>
    <?= $form->field($model, $field)->end(); ?>
    
	<!-- Charge value -->
    <?php $field = 'chargevalue'; ?>
    <?= $form->field($model, $field)->begin(); ?>
        <?= Html::activeLabel($model, $field,['class' => 'control-label col-sm-3']); ?>
        <div class='col-xs-3' style='height: 44px;'>
            <?= MaskMoney::widget([
                'model' => $model,
                'attribute' => $field,
                'pluginOptions' => [
                    'thousands' => ',',
                    'decimal' => '.',
                    'precision' => 2, 
                    'allowZero' => true,
                    'allowNegative' => true,
                    ],
            ]); 
            ?>
        </div>
    <?= $form->field($model, $field)->end(); ?>
    
    <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'taxclaimstatus')->dropDownList([ 'Not claimed' => 'Not claimed', 'Claimed' => 'Claimed', 'Paid' => 'Paid', 'Rejected' => 'Rejected', ], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                    [
                    'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                    'id' => 'submitbutton',
             ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
