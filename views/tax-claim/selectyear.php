<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Families;

/* @var $this yii\web\View */
$this->title = 'Generate Annual Tax Claim';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="taxclaim-selectyear">
    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php
		$lastYear = date("Y") - 1;
		$years = [];
		
		for($year = $lastYear; $year >= $lastYear - 7; $year--) {
			$years[$year] = $year;
		}
		
		$model->taxYear = $lastYear;
		
		$form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]);
	?>

    <p>
        <?= $form->field($model, 'taxYear')->dropDownList($years,
														  ['prompt' => 'Select the tax year to generate the claim for']) ?>
		
		<?= $form->field($model, 'familyId')->dropDownList(
                    ArrayHelper::map(Families::find()->all(), 'familyid','familyname'),
                    ['prompt' => 'Select family']
			    ) 
			?>
			
    </p>
    
    <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> Generate claim', ['class' => 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>

</div>
