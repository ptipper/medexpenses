<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
* @var yii\web\View $this
* @var app\models\TaxClaim $model
*/

$this->title = 'Tax Claim for ' . $model->taxYear;
$this->params['breadcrumbs'][] = ['label' => 'Annual tax claim', 'url' => ['view']];
$this->params['breadcrumbs'][] = 'View';

?>
<div class="taxclaim-view">
    
    <?php echo DetailView::widget([
    'model' => $model,
	'options' => ['class'=>'table table-striped table-bordered detail-view text-right'],
    'attributes' => [
 			[                      
                'label' => 'Family',
                'value' => $model->family->familyname,
            ],
   			'taxYear',
			'genMedExpenses:decimal',
			'prescriptions:decimal',
			'totalMedExpenses:decimal',
			'paidByInsurance:decimal',
			'taxClaimAmount:decimal',
    ],
    ]); ?>

    <hr/>

</div>