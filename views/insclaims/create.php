<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Insclaims */

$this->title = 'Create Insurance Claim';
$this->params['breadcrumbs'][] = ['label' => 'Insurance claims', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insclaims-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'chargesImported' => $chargesImported,
    ]) ?>

</div>
