<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Insclaims */

$this->title = 'Update Insurance Claim: ' . ' ' . $model->claimid;
$this->params['breadcrumbs'][] = ['label' => 'Insurance claims', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->claimid, 'url' => ['view', 'id' => $model->claimid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="insclaims-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'chargesImported' => $chargesImported,
    ]) ?>

</div>
