<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InsclaimsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Insurance claims';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insclaims-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create claim', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'export' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
             'attribute' => 'claimid',
             'label' => 'Claim id'
            ],
            [
             'attribute' => 'policy',
             'value' => 'policy.policynumberdescription',
			 'label' => 'Insurance policy'
            ],
            'claimref',
            [
             'attribute' => 'startdate',
             'label' => 'Start date',
             'format' => 'date',
            ],
            [
             'attribute' => 'enddate',
             'label' => 'End date',
             'format' => 'date',
            ],
            [
             'attribute' => 'claimdate',
             'label' => 'Date submitted',
             'format' => 'date',
            ],
            [
                'class'=>'kartik\grid\BooleanColumn',
                'attribute'=>'settled', 
                'label' => 'Settled',
                'vAlign'=>'middle'
            ], 
            // 'policyid',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
