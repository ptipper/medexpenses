<?php

namespace app\assets;

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Inspolicies;
use kartik\datecontrol\DateControl;
use kartik\tabs\TabsX;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Insclaims */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="insclaims-form">

    <?php $form = ActiveForm::begin(
        [
            'id' => 'insclaimform',
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
        ]); ?>
        
    <!-- Being the Main tab block -->
    <?php $this->beginBlock('main'); ?>
    
    <?= HTML::hiddenInput('claimid', $model->claimid, [ 'id' => 'claimid']) ?>
        
    <?= $form->field($model, 'policyid')->dropDownList(
            ArrayHelper::map(Inspolicies::find()->all(), 'policyid',
            function($model, $defaultValue) {
                return $model->policyno.' - '.$model->description;
            }),
            ['prompt' => 'Select an insurance policy']
	    ) 
    ?>
			

    <?= $form->field($model, 'claimref')->textInput(['maxlength' => 20]) ?>

    <!-- Start date -->
    <?php $field = 'startdate'; ?>
	<?= $form->field($model, $field)->begin(); ?>
        <?= Html::activeLabel($model, $field,['class' => 'control-label col-sm-3']); ?>
        <div class='col-xs-3' style='height: 44px;'>
            <?= DateControl::widget([
                'model' => $model,
                'attribute' => $field,
		        'type'=>DateControl::FORMAT_DATE,
		        'ajaxConversion'=>false,
		        'options' => [
		            // 'id' => 'chargedate_input',
		            'pluginOptions' => [
		                'autoclose' => true,
		            ],
			    ],
		    ]); 
            ?>
        </div>
    <?= $form->field($model, $field)->end(); ?>
            
    <!-- End date -->
    <?php $field = 'enddate'; ?>
	<?= $form->field($model, $field)->begin(); ?>
        <?= Html::activeLabel($model, $field,['class' => 'control-label col-sm-3']); ?>
        <div class='col-xs-3' style='height: 44px;'>
            <?= DateControl::widget([
                'model' => $model,
                'attribute' => $field,
		        'type'=>DateControl::FORMAT_DATE,
		        'ajaxConversion'=>false,
		        'options' => [
		            // 'id' => 'chargedate_input',
		            'pluginOptions' => [
		                'autoclose' => true,
		            ],
			    ],
		    ]); 
            ?>
        </div>
    <?= $form->field($model, $field)->end(); ?>
            
    <!-- Claim date -->
    <?php $field = 'claimdate'; ?>
	<?= $form->field($model, $field)->begin(); ?>
        <?= Html::activeLabel($model, $field,['class' => 'control-label col-sm-3']); ?>
        <div class='col-xs-3' style='height: 44px;'>
            <?= DateControl::widget([
                'model' => $model,
                'attribute' => $field,
		        'type'=>DateControl::FORMAT_DATE,
		        'ajaxConversion'=>false,
		        'options' => [
		            // 'id' => 'chargedate_input',
		            'pluginOptions' => [
		                'autoclose' => true,
		            ],
			    ],
		    ]); 
            ?>
        </div>
    <?= $form->field($model, $field)->end(); ?>
            
    <?= $form->field($model, 'settled')->checkbox([
            'label' => 'Claim settled',
         ]
    ) ?>
    
    <!-- End of Main tab block -->
    <?php $this->endBlock(); ?>
    
    <!-- Being the Charges tab block -->
    <?php 
        $this->beginBlock('charges');
     ?>
    
    <div  style="padding-bottom: 10px;">
        <?php 
            $currentView = ( $model->isNewRecord ? 'create' : 'update' );
            
            echo Html::Button('<span class="glyphicon glyphicon-download"></span> Import charges', 
                [
                    'class' => 'btn btn-success',
                    'id' => 'submit-import',
                    'name' => 'submit-import',
                    'href' => 'index.php?r=insclaims/import',
                    'data-on-done' => 'simpleDone',
                    'data-form-id' => 'insclaimform',
                ]);
            
            $importCountText = 'Click the button to attach eligible medical charges to this claim.';
            echo '<span id="import-count" style="padding-left: 10px;">'. $importCountText . '</span>';
            
         ?>
    </div>
    
    <?php 
        
        echo GridView::widget([
        // 'id' => 'imported-fees-grid',
        'dataProvider' => $model->chargesProvider,
        'export' => false,
        'condensed' => true,
        'pjax'=>true,
        'pjaxSettings'=>[
            // 'neverTimeout'=>true,
            'options' => ['id' => 'imported-fees-grid'],
        ],
        'columns' => [
        
			[
             'attribute' => 'chargeid',
             'value' => 'chargeid',
			 'label' => 'Charge id'
            ],
            [
             'attribute' => 'familymember',
             'value' => 'familymember.name',
			 'label' => 'Family member'
            ],
            [
             'attribute' => 'practitioner',
             'value' => 'practitioner.titlefullname',
			 'label' => 'Practitioner'
            ],
            [
             'attribute' => 'chargedate',
             'value' => 'chargedate',
			 'label' => 'Date',
             'format' => 'date',
            ],
            [
             'class' => '\kartik\grid\DataColumn',
             'attribute' => 'chargevalue',
             'hAlign' => 'right',
             'value' => 'chargevalue',
			 'label' => 'Value',
              'format' => ['decimal',2],
            ],
            'notes:ntext',
        ],
    ]); ?>
    
    
        
    <!-- End of Charges tab block -->
    <?php 
        $this->endBlock(); 
     ?>
        
    <!-- Display the tabs -->
    <?= TabsX::widget(
            [
                'position'=>TabsX::POS_ABOVE,
                'encodeLabels'=>false,
                'bordered'=>true,
                'height'=>TabsX::SIZE_LARGE,
                'items' => 
                     [ 
                             [
                                'label'   => 'Main',
                                'content' => $this->blocks['main'],
                                // 'active'  => true,
                             ], 
                             [
                                'label'   => 'Charges',
                                'content' => $this->blocks['charges'],
                             ], 
                     ],
            ]
        );
    
    ?>
    
    <p/>
    
    <div class="form-group">
        <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> ' . ( $model->isNewRecord ? 'Create' : 'Update' ), 
            [
                'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
                'name' => 'submit-save',
                
            ]) 
        ?>
    </div>

    <?php 
        ActiveForm::end();

        // Bind the 'Import' checkbox to the handleAjaxLink Ajax event handler in ajax.js
        $this->registerJs("$('#submit-import').click(handleAjaxLink);", \yii\web\View::POS_READY);
        
    ?>

</div>
