<?php

namespace app\assets;

use yii\helpers\Html;
use kartik\detail\DetailView;
use kartik\tabs\TabsX;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Insclaims */

$this->title = $model->claimref;
$this->params['breadcrumbs'][] = ['label' => 'Insurance claims', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insclaims-view">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <!-- Being the Main tab block -->
    <?php $this->beginBlock('main'); ?>
    
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->claimid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->claimid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'claimid',
            [
            'attribute'=>'policyid',
            'value'=>$model->policy->description,
            ],
            'claimref',
            'startdate:date',
            'enddate:date',
            'claimdate:date',
            'settled:boolean',
        ],
    ]) ?>
    
    <!-- End of Main tab block -->
    <?php $this->endBlock(); ?>
    
    <!-- Begin the Claim Summary tab block -->
    <?php $this->beginBlock('summary'); ?>
    
     <?php 
        
        echo GridView::widget([
        'dataProvider' => $model->claimSummaryProvider(),
        'export' => false,
        'condensed' => true,
        'pjax'=>true,
        'pjaxSettings'=>[
            // 'neverTimeout'=>true,
            'options' => ['id' => 'claim-summary-grid'],
        ],
		'showPageSummary' => true,
        'columns' => [
        
			[
             'attribute' => 'claimcodedesc',
             'value' => 'claimcodedesc',
			 'label' => 'Claim code',
			 'pageSummary' => 'Totals',
            ],
            [
             'class' => '\kartik\grid\DataColumn',
             'attribute' => 'visits',
             'hAlign' => 'right',
             'value' => 'visits',
			 'label' => 'Visits',
			 'pageSummary' => true,
            ],
            [
             'class' => '\kartik\grid\DataColumn',
             'attribute' => 'chargevalue',
             'hAlign' => 'right',
             'value' => 'chargevalue',
			 'label' => 'Total fee value',
              'format' => ['decimal',2],
			 'pageSummary' => true,
            ],
            [
             'class' => '\kartik\grid\DataColumn',
             'attribute' => 'payable',
             'hAlign' => 'right',
             'value' => 'payable',
			 'label' => 'Amount payable',
              'format' => ['decimal',2],
			 'pageSummary' => true,
            ],
        ],
    ]); ?>
    
    <!-- End of Claim Summary tab block -->
    <?php $this->endBlock(); ?>
    
    <!-- Display the tabs -->
    <?= TabsX::widget(
            [
                'position'=>TabsX::POS_ABOVE,
                'encodeLabels'=>false,
                'bordered'=>true,
                'height'=>TabsX::SIZE_LARGE,
                'items' => 
                     [ 
                             [
                                'label'   => 'Main',
                                'content' => $this->blocks['main'],
                                // 'active'  => true,
                             ], 
                             [
                                'label'   => 'Summary',
                                'content' => $this->blocks['summary'],
                             ], 
                     ],
            ]
        );
    
    ?>

</div>
