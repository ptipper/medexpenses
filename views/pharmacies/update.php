<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Pharmacies $model
 */

$this->title = 'Pharmacies Update ' . $model->name . '';
$this->params['breadcrumbs'][] = ['label' => 'Pharmacies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->name, 'url' => ['view', 'pharmacyid' => $model->pharmacyid]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="pharmacies-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'pharmacyid' => $model->pharmacyid], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
