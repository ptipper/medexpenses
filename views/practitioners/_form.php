<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Practtypes;

/**
* @var yii\web\View $this
* @var app\models\Practitioners $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="practitioners-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>
            
			<?= $form->field($model, 'title')->dropDownList([
															 'Dr.' => 'Dr.',
															 'Mr.' => 'Mr.',
															 'Ms.' => 'Ms.',
															 'Prof.' => 'Prof.',
															 'N/A' => 'N/A',
															 ],
															[
															 'prompt' => ''
															]) ?>
			<?= $form->field($model, 'firstname')->textInput(['maxlength' => 20]) ?>
			<?= $form->field($model, 'surname')->textInput(['maxlength' => 20]) ?>
			<?= $form->field($model, 'practtypeid')->dropDownList(
                    ArrayHelper::map(Practtypes::find()->all(), 'practtypeid','description'),
                    ['prompt' => 'Select a practitioner']
			    ) 
			?>
			
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    \yii\bootstrap\Tabs::widget(
                 [
                   'encodeLabels' => false,
                     'items' => [ [
    'label'   => 'Medical practitioners',
    'content' => $this->blocks['main'],
    'active'  => true,
], ]
                 ]
    );
    ?>
        <hr/>

        <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> '.($model->isNewRecord ? 'Create' : 'Save'), ['class' => $model->isNewRecord ?
        'btn btn-primary' : 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
