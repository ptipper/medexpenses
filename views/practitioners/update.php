<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Practitioners $model
 */

$this->title = 'Practitioners Update ' . $model->title . '';
$this->params['breadcrumbs'][] = ['label' => 'Practitioners', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->title, 'url' => ['view', 'practitionerid' => $model->practitionerid]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="practitioners-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'practitionerid' => $model->practitionerid], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
