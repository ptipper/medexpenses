<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\PractitionersSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="practitioners-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'practitionerid') ?>

		<?= $form->field($model, 'title') ?>

		<?= $form->field($model, 'firstname') ?>

		<?= $form->field($model, 'surname') ?>

		<?= $form->field($model, 'practtypeid') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
