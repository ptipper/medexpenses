<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var app\models\Practtypes $model
*/

$this->title = 'Practtypes View ' . $model->practtypeid . '';
$this->params['breadcrumbs'][] = ['label' => 'Practitioner types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->practtypeid, 'url' => ['view', 'practtypeid' => $model->practtypeid]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="practtypes-view">

    <p class='pull-left'>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Edit', ['update', 'practtypeid' => $model->practtypeid],
        ['class' => 'btn btn-info']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Type', ['create'], ['class' => 'btn
        btn-success']) ?>
    </p>

        <p class='pull-right'>
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['index'], ['class'=>'btn btn-default']) ?>
    </p><div class='clearfix'></div> 

    
    <h3>
        <?= $model->practtypeid ?>    </h3>


    <?php $this->beginBlock('app\models\Practtypes'); ?>

    <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    			'practtypeid',
    			'description',
    			[                      
                    'label' => 'Insurance claim code',
                    'value' => $model->claimcode->description,
                ],
                [                      
                    'label' => 'Taxation expense type',
                    'value' => $model->taxexptype->description,
                ],
            ],
    ]); ?>

    <hr/>

    <?php echo Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['delete', 'practtypeid' => $model->practtypeid],
    [
    'class' => 'btn btn-danger',
    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
    'data-method' => 'post',
    ]); ?>

    <?php $this->endBlock(); ?>


    
<?php $this->beginBlock('Practitioners'); ?>
<p class='pull-right'>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-list"></span> List All Practitioners',
            ['practitioners/index'],
            ['class'=>'btn text-muted btn-xs']
        ) ?>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-plus"></span> New Practitioner',
            ['practitioners/create', 'Practitioner'=>['practtypeid'=>$model->practtypeid]],
            ['class'=>'btn btn-success btn-xs']
        ) ?>
</p><div class='clearfix'></div>
<?php $this->endBlock() ?>


    <?=
    \yii\bootstrap\Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<span class="glyphicon glyphicon-asterisk"></span> Practitioner types',
    'content' => $this->blocks['app\models\Practtypes'],
    'active'  => true,
],[
    'label'   => '<small><span class="glyphicon glyphicon-paperclip"></span> Practitioners</small>',
    'content' => $this->blocks['Practitioners'],
    'active'  => false,
], ]
                 ]
    );
    ?></div>
