<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Practtypes $model
 */

$this->title = 'Practtypes Update ' . $model->practtypeid . '';
$this->params['breadcrumbs'][] = ['label' => 'Practitioner types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->practtypeid, 'url' => ['view', 'practtypeid' => $model->practtypeid]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="practtypes-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'practtypeid' => $model->practtypeid], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
