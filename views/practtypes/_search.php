<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\PracttypesSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="practtypes-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

		<?= $form->field($model, 'practtypeid') ?>

		<?= $form->field($model, 'description') ?>

		<?= $form->field($model, 'claimcodeid') ?>

		<?= $form->field($model, 'taxexptypeid') ?>

		<div class="form-group">
			<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
			<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
		</div>

	<?php ActiveForm::end(); ?>

</div>
