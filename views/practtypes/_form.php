<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Insclaimcodes;
use app\models\Taxexptypes;

/**
* @var yii\web\View $this
* @var app\models\Practtypes $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="practtypes-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>
            
			<?= $form->field($model, 'claimcodeid')->dropDownList(
                    ArrayHelper::map(Insclaimcodes::find()->all(), 'claimcodeid','description'),
                    ['prompt' => 'Select a insurance claim code']
			    ) 
			?>
			<?= $form->field($model, 'taxexptypeid')->dropDownList(
                    ArrayHelper::map(Taxexptypes::find()->all(), 'taxexptypeid','description'),
                    ['prompt' => 'Select a taxation expense type']
			    ) 
			?>
			<?= $form->field($model, 'description')->textInput(['maxlength' => 120]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    \yii\bootstrap\Tabs::widget(
                 [
                   'encodeLabels' => false,
                     'items' => [ [
    'label'   => 'Practioner types',
    'content' => $this->blocks['main'],
    'active'  => true,
], ]
                 ]
    );
    ?>
        <hr/>

        <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> '.($model->isNewRecord ? 'Create' : 'Save'), ['class' => $model->isNewRecord ?
        'btn btn-primary' : 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
