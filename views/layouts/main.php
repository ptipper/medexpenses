<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$companyName = 'Paul Tipper';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => $this->title,
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => 'Home', 'url' => ['/site/index']],
                    [
                        'label' => 'Tables',
                        'items' => [
                             '<li class="dropdown-header">Basic tables</li>',
                                 ['label' => 'Medical practitioners', 'url' => ['/practitioners/index']],
                                 ['label' => 'Pharmacies', 'url' => ['/pharmacies/index']],
                                 ['label' => 'Tax expense types', 'url' => ['/taxexptypes/index']],
                                 ['label' => 'Insurance companies', 'url' => ['/insurancecos/index']],
                                 ['label' => 'Insurance claim codes', 'url' => ['/insclaimcodes/index']],
                                 ['label' => 'Insurance policies', 'url' => ['/inspolicies/index']],
                                 ['label' => 'Insurance policy rules', 'url' => ['/insccrules/index']],
                                 ['label' => 'Families', 'url' => ['/families/index']],
                                 ['label' => 'Family members', 'url' => ['/familymembers/index']],
                                 ['label' => 'Practitioner types', 'url' => ['/practtypes/index']],
                            /*
                             '<li class="divider"></li>',
                             '<li class="dropdown-header">Transaction tables</li>',
                                 ['label' => 'Import pages', 'url' => ['/imppages/index']],
                                 ['label' => 'Imported transactions', 'url' => ['/imptrans/index']],
                            */
                        ],
                    ],
                    ['label' => 'About', 'url' => ['/site/about']],
                    Yii::$app->user->isGuest ?
                        ['label' => 'Login', 'url' => ['/site/login']] :
                        ['label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                            'url' => ['/site/logout'],
                            'linkOptions' => ['data-method' => 'post']],
                ],
            ]);
            NavBar::end();
        ?>

        <div class="container">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; <?= $companyName ?> <?= date('Y') ?></p>
            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
