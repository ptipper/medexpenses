<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Insurancecos;

/* @var $this yii\web\View */
/* @var $model app\models\Insclaimcodes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="insclaimcodes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'insurancecoid')->dropDownList(
            ArrayHelper::map(Insurancecos::find()->all(), 'insurancecoid','name'),
            ['prompt' => 'Select insurance company']) ?>

    <?= $form->field($model, 'claimcode')->textInput(['maxlength' => 4]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => 120]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
