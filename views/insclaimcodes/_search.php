<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\InsclaimcodesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="insclaimcodes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'claimcodeid') ?>

    <?= $form->field($model, 'insurancecoid') ?>

    <?= $form->field($model, 'claimcode') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'maxvisits') ?>

    <?php // echo $form->field($model, 'maxclaimvalue') ?>

    <?php // echo $form->field($model, 'percentcover') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
