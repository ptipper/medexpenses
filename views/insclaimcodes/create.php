<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Insclaimcodes */

$this->title = 'Create Insurance Claim Codes';
$this->params['breadcrumbs'][] = ['label' => 'Insurance Claim Codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insclaimcodes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
