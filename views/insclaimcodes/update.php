<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Insclaimcodes */

$this->title = 'Update Insurance Claim Codes: ' . ' ' . $model->claimcodeid;
$this->params['breadcrumbs'][] = ['label' => 'Insurance Claim Codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->claimcodeid, 'url' => ['view', 'id' => $model->claimcodeid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="insclaimcodes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
