<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Insclaimcodes */

$this->title = $model->claimcodeid;
$this->params['breadcrumbs'][] = ['label' => 'Insurance Claim Codes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insclaimcodes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->claimcodeid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->claimcodeid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'claimcodeid',
            'insurancecoid',
            'claimcode',
            'description',
        ],
    ]) ?>

</div>
