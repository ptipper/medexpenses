<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Insccrules */

$this->title = 'Update Insurance Policy Rule: ' . ' ' . $model->claimcodeid;
$this->params['breadcrumbs'][] = ['label' => 'Insurance policy rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->claimcodeid, 'url' => ['view', 'claimcodeid' => $model->claimcodeid, 'policyid' => $model->policyid]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="insccrules-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
