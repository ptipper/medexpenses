<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\InsccrulesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="insccrules-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'claimcodeid') ?>

    <?= $form->field($model, 'policyid') ?>

    <?= $form->field($model, 'maxvisits') ?>

    <?= $form->field($model, 'maxclaimvalue') ?>

    <?= $form->field($model, 'percentcover') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
