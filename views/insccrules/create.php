<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Insccrules */

$this->title = 'Create Insurance Policy Rule';
$this->params['breadcrumbs'][] = ['label' => 'Insurance policy rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insccrules-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
