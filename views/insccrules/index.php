<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InsccrulesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Insurance policy rules';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insccrules-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('New rule', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'export' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
             'attribute' => 'policy',
             'value' => 'policy.description',
			 'label' => 'Insurance policy'
            ],
            [
             'attribute' => 'claimcode',
             'value' => 'claimcode.description',
			 'label' => 'Claim code'
            ],
            [
             'class' => '\kartik\grid\DataColumn',
             'attribute' => 'maxvisits',
             'hAlign' => 'right',
             'value' => 'maxvisits',
			 'label' => 'Maximum visits/claim',
              'format' => 'integer',
            ],
            [
             'class' => '\kartik\grid\DataColumn',
             'attribute' => 'maxclaimvalue',
             'hAlign' => 'right',
             'value' => 'maxclaimvalue',
			 'label' => 'Maximum claim value',
              'format' => ['decimal',2],
            ],
            [
             'class' => '\kartik\grid\DataColumn',
             'attribute' => 'percentcover',
             'hAlign' => 'right',
             'value' => 'percentcover',
			 'label' => 'Percentage cover',
              'format' => 'percent',
            ],
            [
             'class' => '\kartik\grid\DataColumn',
             'attribute' => 'maxpaypppa',
             'hAlign' => 'right',
             'value' => 'maxpaypppa',
			 'label' => 'Maximum annual value',
              'format' => ['decimal',2],
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
