<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Inspolicies;
use app\models\Insclaimcodes;

/* @var $this yii\web\View */
/* @var $model app\models\Insccrules */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="insccrules-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- Insurance policy -->
	<?= $form->field($model, 'policyid')->dropDownList(
            ArrayHelper::map(Inspolicies::find()->all(), 'policyid','description'),
            ['prompt' => 'Select insurance policy']
	    ) 
	?>
	
	<!-- Insurance claim code -->
	<?= $form->field($model, 'claimcodeid')->dropDownList(
            ArrayHelper::map(Insclaimcodes::find()->all(), 'claimcodeid','description'),
            ['prompt' => 'Select claim code']
	    ) 
	?>
	
	<?= $form->field($model, 'maxvisits')->textInput() ?>

    <?= $form->field($model, 'maxclaimvalue')->textInput() ?>
	
	<?= $form->field($model, 'percentcover')->textInput() ?>

    <?= $form->field($model, 'maxpaypppa')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
