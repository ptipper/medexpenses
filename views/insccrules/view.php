<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Insccrules */

$this->title = $model->claimcodeid;
$this->params['breadcrumbs'][] = ['label' => 'Insurance policy rules', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insccrules-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'claimcodeid' => $model->claimcodeid, 'policyid' => $model->policyid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'claimcodeid' => $model->claimcodeid, 'policyid' => $model->policyid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'policyid',
            'claimcodeid',
            'maxvisits',
            'maxclaimvalue',
            'percentcover',
            'maxpaypppa',
        ],
    ]) ?>

</div>
