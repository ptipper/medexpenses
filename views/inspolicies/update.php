<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Inspolicies $model
 */

$this->title = 'Insurance Policies Update ' . $model->policyid . '';
$this->params['breadcrumbs'][] = ['label' => 'Insurance Policies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->policyid, 'url' => ['view', 'policyid' => $model->policyid]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="inspolicies-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'policyid' => $model->policyid], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
