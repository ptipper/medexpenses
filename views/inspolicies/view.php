<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/**
* @var yii\web\View $this
* @var app\models\Inspolicies $model
*/

$this->title = 'Insurance Policies View ' . $model->policyid . '';
$this->params['breadcrumbs'][] = ['label' => 'Insurance Policies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->policyid, 'url' => ['view', 'policyid' => $model->policyid]];
$this->params['breadcrumbs'][] = 'View';
?>
<div class="inspolicies-view">

    <p class='pull-left'>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Edit', ['update', 'policyid' => $model->policyid],
        ['class' => 'btn btn-info']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> New Inspolicies', ['create'], ['class' => 'btn
        btn-success']) ?>
    </p>

        <p class='pull-right'>
        <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['index'], ['class'=>'btn btn-default']) ?>
    </p><div class='clearfix'></div> 

    
    <h3>
        <?= $model->policyid ?>    </h3>


    <?php $this->beginBlock('app\models\Inspolicies'); ?>

    <?php echo DetailView::widget([
    'model' => $model,
    'attributes' => [
    			'policyid',
			'insuranceco.name',
			'policyno',
			'description',
    ],
    ]); ?>

    <hr/>

    <?php echo Html::a('<span class="glyphicon glyphicon-trash"></span> Delete', ['delete', 'policyid' => $model->policyid],
    [
    'class' => 'btn btn-danger',
    'data-confirm' => Yii::t('app', 'Are you sure to delete this item?'),
    'data-method' => 'post',
    ]); ?>

    <?php $this->endBlock(); ?>


    
<?php $this->beginBlock('Families'); ?>
<p class='pull-right'>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-list"></span> List All Families',
            ['families/index'],
            ['class'=>'btn text-muted btn-xs']
        ) ?>
  <?= \yii\helpers\Html::a(
            '<span class="glyphicon glyphicon-plus"></span> New Family',
            ['families/create', 'Family'=>['policyid'=>$model->policyid]],
            ['class'=>'btn btn-success btn-xs']
        ) ?>
</p><div class='clearfix'></div>
<?php $this->endBlock() ?>


    <?=
    \yii\bootstrap\Tabs::widget(
                 [
                     'id' => 'relation-tabs',
                     'encodeLabels' => false,
                     'items' => [ [
    'label'   => '<span class="glyphicon glyphicon-asterisk"></span> Inspolicies',
    'content' => $this->blocks['app\models\Inspolicies'],
    'active'  => true,
],[
    'label'   => '<small><span class="glyphicon glyphicon-paperclip"></span> Families</small>',
    'content' => $this->blocks['Families'],
    'active'  => false,
], ]
                 ]
    );
    ?></div>
