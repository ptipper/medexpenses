<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Insurancecos;

/**
* @var yii\web\View $this
* @var app\models\Inspolicies $model
* @var yii\widgets\ActiveForm $form
*/
?>

<div class="inspolicies-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal', 'enableClientValidation' => false]); ?>

    <div class="">
        <?php echo $form->errorSummary($model); ?>
        <?php $this->beginBlock('main'); ?>

        <p>
            
			<?= $form->field($model, 'insurancecoid')->dropDownList(
                    ArrayHelper::map(Insurancecos::find()->all(), 'insurancecoid','name'),
                    ['prompt' => 'Select insurance company']
			    ) 
			?>
			<?= $form->field($model, 'policyno')->textInput(['maxlength' => 20]) ?>
			<?= $form->field($model, 'description')->textInput(['maxlength' => 120]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
        <?=
    \yii\bootstrap\Tabs::widget(
                 [
                   'encodeLabels' => false,
                     'items' => [ [
    'label'   => 'Insurance Policies',
    'content' => $this->blocks['main'],
    'active'  => true,
], ]
                 ]
    );
    ?>
        <hr/>

        <?= Html::submitButton('<span class="glyphicon glyphicon-check"></span> '.($model->isNewRecord ? 'Create' : 'Save'), ['class' => $model->isNewRecord ?
        'btn btn-primary' : 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
