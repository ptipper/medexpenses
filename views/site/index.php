<?php
/* @var $this yii\web\View */
$this->title = 'Medical Expenses';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>

        <p class="lead">A Yii application for storing medical expenses and prescription charges, and for submitting health insurance claims and tax returns.</p>

        <p><a class="btn btn-lg btn-success" href="index.php?r=gii">Gii Code Generator</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-3">
                <h2>Professional Fees</h2>

                <p class="landingpg-paragraph">Enter and maintain the details of professional fees and other, non-prescription medical charges.</p>

                <p><a class="btn btn-primary" href="index.php?r=profees/index"">Enter fees &raquo;</a></p>
            </div>
            <div class="col-lg-3">
                <h2>Prescriptions</h2>

                <p class="landingpg-paragraph">Enter and maintain the details of prescription charges.</p>

                <p><a class="btn btn-primary" href="index.php?r=prescriptions/index"">Enter prescriptions &raquo;</a></p>
            </div>
            <div class="col-lg-3">
                <h2>Insurance Claims</h2>

                <p class="landingpg-paragraph">Generate and maintain insurance claims.</p>

                <p><a class="btn btn-primary" href="index.php?r=insclaims/index">Insurance claims &raquo;</a></p>
            </div>
            <div class="col-lg-3">
                <h2>Tax Claims</h2>

                <p class="landingpg-paragraph">Generate annual tax claims.</p>

                <p><a class="btn btn-primary" href="index.php?r=tax-claim/view">Annual tax claims &raquo;</a></p>
        </div>

    </div>
</div>
