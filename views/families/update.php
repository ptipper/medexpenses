<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Families $model
 */

$this->title = 'Families Update ' . $model->familyid . '';
$this->params['breadcrumbs'][] = ['label' => 'Families', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->familyid, 'url' => ['view', 'familyid' => $model->familyid]];
$this->params['breadcrumbs'][] = 'Edit';
?>
<div class="families-update">

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view', 'familyid' => $model->familyid], ['class' => 'btn btn-info']) ?>
    </p>

	<?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
